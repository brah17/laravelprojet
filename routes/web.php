<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test','RedirectionRoleController@role');
Route::get('/role','RoleController@index');
Route::post('/role/add','RoleController@role');
Route::get('/organisateur','OrganisateurControlller@index');
Route::get('/admin','AdminController@index');
Route::get('/admins/list','AdminController@list');
Route::post('/admins/create','AdminController@create');
Route::get('/admins/read/{id}','AdminController@read');
Route::get('/admins/add','AdminController@addAdmin');
Route::get('/admins/delete','AdminController@delete');
Route::get('/admins/pagedelete/{id}','AdminController@pagedelete');
Route::post('/admins/remove','AdminController@remove');
Route::get('/admins/update','AdminController@update');
Route::post('/admins/edit','AdminController@edit');
Route::get('/admins/pageupdate/{id}','AdminController@pageupdate');
Route::get('/admins/search','AdminController@search');
Route::post('/admins/recherche','AdminController@recherche');

Route::get('/lieus','LieusController@index');
Route::post('/lieus/create','LieusController@create');
Route::get('/lieus/read/{id}','LieusController@read');
Route::get('/lieus/add','LieusController@add');
Route::get('/lieus/delete','LieusController@delete');
Route::get('/lieus/pagedelete/{id}','LieusController@pagedelete');
Route::post('/lieus/remove','LieusController@remove');
Route::get('/lieus/update','LieusController@update');
Route::post('/lieus/edit','LieusController@edit');
Route::get('/lieus/pageupdate/{id}','LieusController@pageupdate');
Route::get('/lieus/search','LieusController@search');
Route::post('/lieus/recherche','LieusController@recherche');

Route::get('/events','EventController@index');
Route::post('/events/create','EventController@create');
Route::get('/events/read/{id}','EventController@read');
Route::get('/events/add','EventController@addEvent');
Route::get('/events/delete','EventController@delete');
Route::get('/events/pagedelete/{id}','EventController@pagedelete');
Route::post('/events/remove','EventController@remove');
Route::get('/events/update','EventController@update');
Route::post('/events/edit','EventController@edit');
Route::get('/events/pageupdate/{id}','EventController@pageupdate');
Route::get('/events/calendar','EventController@calendar');
Route::get('/events/search','EventController@search');
Route::post('/events/recherche','EventController@recherche');

Route::get('/organisateurs','OrganisateurController@index');
Route::get('/organisateurs/list','OrganisateurController@list');
Route::post('/organisateurs/create','OrganisateurController@create');
Route::get('/organisateurs/read/{id}','OrganisateurController@read');
Route::get('/organisateurs/add','OrganisateurController@addOrganisateur');
Route::get('/organisateurs/delete','OrganisateurController@delete');
Route::get('/organisateurs/pagesupprimer/{id}','OrganisateurController@pagedelete');
Route::post('/organisateurs/remove','OrganisateurController@remove');
Route::get('/organisateurs/update','OrganisateurController@update');
Route::post('/organisateurs/edit','OrganisateurController@edit');
Route::get('/organisateurs/pagemodifier/{id}','OrganisateurController@pageupdate');
Route::get('/organisateurs/search','OrganisateurController@search');
Route::post('/organisateurs/recherche','OrganisateurController@recherche');

Route::get('/mandataires','MandataireController@index');
Route::get('/mandataires/list','MandataireController@list');
Route::post('/mandataires/create','MandataireController@create');
Route::get('/mandataires/read/{id}','MandataireController@read');
Route::get('/mandataires/add','MandataireController@addMandataire');
Route::get('/mandataires/delete','MandataireController@delete');
Route::get('/mandataires/pagedelete/{id}','MandataireController@pagedelete');
Route::post('/mandataires/remove','MandataireController@remove');
Route::get('/mandataires/update','MandataireController@update');
Route::post('/mandataires/edit','MandataireController@edit');
Route::get('/mandataires/pageupdate/{id}','MandataireController@pageupdate');
Route::get('/mandataires/search','MandataireController@search');
Route::post('/mandataires/recherche','MandataireController@recherche');

Route::get('/producteurs','ProducteurController@index');
Route::get('/producteurs/list','ProducteurController@list');
Route::post('/producteurs/create','ProducteurController@create');
Route::get('/producteurs/read/{id}','ProducteurController@read');
Route::get('/producteurs/add','ProducteurController@addProducteur');
Route::get('/producteurs/delete','ProducteurController@delete');
Route::get('/producteurs/pagedelete/{id}','ProducteurController@pagedelete');
Route::post('/producteurs/remove','ProducteurController@remove');
Route::get('/producteurs/update','ProducteurController@update');
Route::post('/producteurs/edit','ProducteurController@edit');
Route::get('/producteurs/pageupdate/{id}','ProducteurController@pageupdate');
Route::get('/producteurs/search','ProducteurController@search');
Route::post('/producteurs/recherche','ProducteurController@recherche');

Route::get('/intermittants','IntermittantController@index');
Route::get('/intermittants/list','IntermittantController@list');
Route::post('/intermittants/create','IntermittantController@create');
Route::get('/intermittants/read/{id}','IntermittantController@read');
Route::get('/intermittants/add','IntermittantController@addIntermittant');
Route::get('/intermittants/delete','IntermittantController@delete');
Route::get('/intermittants/pagedelete/{id}','IntermittantController@pagedelete');
Route::post('/intermittants/remove','IntermittantController@remove');
Route::get('/intermittants/update','IntermittantController@update');
Route::post('/intermittants/edit','IntermittantController@edit');
Route::get('/intermittants/pageupdate/{id}','SuperviseurController@pageupdate');
Route::get('/intermittants/search','IntermittantController@search');
Route::post('/intermittants/recherche','IntermittantController@recherche');

Route::get('/superviseurs','SuperviseurController@index');
Route::get('/superviseurs/list','SuperviseurController@list');
Route::post('/superviseurs/create','SuperviseurController@create');
Route::get('/superviseurs/read/{id}','SuperviseurController@read');
Route::get('/superviseurs/add','SuperviseurController@addSuperviseur');
Route::get('/superviseurs/delete','SuperviseurController@delete');
Route::get('/superviseurs/pagedelete/{id}','SuperviseurController@pagedelete');
Route::post('/superviseurs/remove','SuperviseurController@remove');
Route::get('/superviseurs/update','SuperviseurController@update');
Route::post('/superviseurs/edit','SuperviseurController@edit');
Route::get('/superviseurs/pageupdate/{id}','SuperviseurController@pageupdate');
Route::get('/superviseurs/search','SuperviseurController@search');
Route::post('/superviseurs/recherche','SuperviseurController@recherche');

Route::get('/agents','AgentController@index');
Route::get('/agents/list','AgentController@list');
Route::post('/agents/create','AgentController@create');
Route::get('/agents/read/{id}','AgentController@read');
Route::get('/agents/add','AgentController@addAgent');
Route::get('/agents/delete','AgentController@delete');
Route::get('/agents/pagedelete/{id}','AgentController@pagedelete');
Route::post('/agents/remove','AgentController@remove');
Route::get('/agents/update','AgentController@update');
Route::post('/agents/edit','AgentController@edit');
Route::get('/agents/pageupdate/{id}','AgentController@pageupdate');
Route::get('/agents/search','AgentController@search');
Route::post('/agents/recherche','AgentController@recherche');

Route::get('/controleurs','ControleurController@index');
Route::get('/controleurs/list','ControleurController@list');
Route::post('/controleurs/create','ControleurController@create');
Route::get('/controleurs/read/{id}','ControleurController@read');
Route::get('/controleurs/add','ControleurController@addControleur');
Route::get('/controleurs/delete','ControleurController@delete');
Route::get('/controleurs/pagedelete/{id}','ControleurController@pagedelete');
Route::post('/controleurs/remove','ControleurController@remove');
Route::get('/controleurs/update','ControleurController@update');
Route::post('/controleurs/edit','ControleurController@edit');
Route::get('/controleurs/pageupdate/{id}','ControleurController@pageupdate');
Route::get('/controleurs/search','ControleurController@search');
Route::post('/controleurs/recherche','ControleurController@recherche');

Route::get('/spectateurs','SpectateurController@index');
Route::get('/spectateurs/list','SpectateurController@list');
Route::post('/spectateurs/create','SpectateurController@create');
Route::get('/spectateurs/read/{id}','SpectateurController@read');
Route::get('/spectateurs/add','SpectateurController@addSpectateur');
Route::get('/spectateurs/delete','SpectateurController@delete');
Route::get('/spectateurs/pagedelete/{id}','SpectateurController@pagedelete');
Route::post('/spectateurs/remove','SpectateurController@remove');
Route::get('/spectateurs/update','SpectateurController@update');
Route::post('/spectateurs/edit','SpectateurController@edit');
Route::get('/spectateurs/pageupdate/{id}','SpectateurController@pageupdate');
Route::get('/spectateurs/search','SpectateurController@search');
Route::post('/spectateurs/recherche','SpectateurController@recherche');

Route::get('/agences','AgenceController@index');
Route::get('/agences/list','AgenceController@list');
Route::post('/agences/create','AgenceController@create');
Route::get('/agences/read/{id}','AgenceController@read');
Route::get('/agences/add','AgenceController@addAgence');
Route::get('/agences/delete','AgenceController@delete');
Route::get('/agences/pagedelete/{id}','AgenceController@pagedelete');
Route::post('/agences/remove','AgenceController@remove');
Route::get('/agences/update','AgenceController@update');
Route::post('/agences/edit','AgenceController@edit');
Route::get('/agences/pageupdate/{id}','AgenceController@pageupdate');
Route::get('/agences/search','AgenceController@search');
Route::post('/agences/recherche','AgenceController@recherche');

Route::get('/distributeurs','DestController@index');
Route::get('/distributeurs/list','DestController@list');
Route::post('/distributeurs/create','DestController@create');
Route::get('/distributeurs/read/{id}','DestController@read');
Route::get('/distributeurs/add','DestController@addDestributeur');
Route::get('/distributeurs/delete','DestController@delete');
Route::get('/distributeurs/pagedelete/{id}','DestController@pagedelete');
Route::post('/distributeurs/remove','DestController@remove');
Route::get('/distributeurs/update','DestController@update');
Route::post('/distributeurs/edit','DestController@edit');
Route::get('/distributeurs/pageupdate/{id}','DestController@pageupdate');
Route::get('/distributeurs/update','DestController@search');
Route::post('/distributeurs/edit','DestController@recherche');

Route::get('/revendeurs','RevendeurController@index');
Route::get('/revendeurs/list','RevendeurController@list');
Route::post('/revendeurs/create','RevendeurController@create');
Route::get('/revendeurs/read/{id}','RevendeurController@read');
Route::get('/revendeurs/add','RevendeurController@addRevendeur');
Route::get('/revendeurs/delete','RevendeurController@delete');
Route::get('/revendeurs/pagedelete/{id}','RevendeurController@pagedelete');
Route::post('/revendeurs/remove','RevendeurController@remove');
Route::get('/revendeurs/update','RevendeurController@update');
Route::post('/revendeurs/edit','RevendeurController@edit');
Route::get('/revendeurs/pageupdate/{id}','RevendeurController@pageupdate');
Route::get('/revendeurs/search','RevendeurController@search');
Route::post('/revendeurs/edit','RevendeurController@recherche');

Route::get('/institutions','InstitutionController@index');
Route::get('/institutions/list','InstitutionController@list');
Route::post('/institutions/create','InstitutionController@create');
Route::get('/institutions/read/{id}','InstitutionController@read');
Route::get('/institutions/add','InstitutionController@addInstitution');
Route::get('/institutions/delete','InstitutionController@delete');
Route::get('/institutions/pagedelete/{id}','InstitutionController@pagedelete');
Route::post('/institutions/remove','InstitutionController@remove');
Route::get('/institutions/update','InstitutionController@update');
Route::post('/institutions/edit','InstitutionController@edit');
Route::get('/institutions/pageupdate/{id}','InstitutionController@pageupdate');
Route::get('/institutions/search','InstitutionController@search');
Route::post('/institutions/recherche','InstitutionController@recherche');

Route::get('/invitations','InvitationController@index');
Route::get('/invitations/list','InvitationController@list');
Route::post('/invitations/create','InvitationController@create');
Route::get('/invitations/read/{id}','InvitationController@read');
Route::get('/invitations/add','InvitationController@addIntermittant');
Route::get('/invitations/delete','InvitationController@delete');
Route::get('/invitations/pagedelete/{id}','InvitationController@pagedelete');
Route::post('/invitations/remove','InvitationController@remove');
Route::get('/invitations/update','InvitationController@update');
Route::post('/invitations/edit','InvitationController@edit');
Route::get('/invitations/pageupdate/{id}','InvitationController@pageupdate');
Route::get('/invitations/search','InvitationController@search');
Route::post('/invitations/recherche','InvitationController@recherche');

Route::get('/tickets','TicketController@index');
Route::get('/tickets/list','TicketController@list');
Route::post('/tickets/create','TicketController@create');
Route::get('/tickets/read/{id}','TicketController@read');
Route::get('/tickets/add','TicketController@addIntermittant');
Route::get('/tickets/delete','TicketController@delete');
Route::get('/tickets/pagedelete/{id}','TicketController@pagedelete');
Route::post('/tickets/remove','TicketController@remove');
Route::get('/tickets/update','TicketController@update');
Route::post('/tickets/edit','TicketController@edit');
Route::get('/tickets/pageupdate/{id}','TicketController@pageupdate');
Route::get('/tickets/search','TicketController@search');
Route::post('/tickets/recherche','TicketController@recherche');

Route::get('/accueils','AccueilController@index');
Route::get('/accueils/list','AccueilController@list');
Route::post('/accueils/create','AccueilController@create');
Route::get('/accueils/read/{id}','AccueilController@read');
Route::get('/accueils/add','AccueilController@addIntermittant');
Route::get('/accueils/delete','AccueilController@delete');
Route::get('/accueils/pagedelete/{id}','AccueilController@pagedelete');
Route::post('/accueils/remove','AccueilController@remove');
Route::get('/accueils/update','AccueilController@update');
Route::post('/accueils/edit','AccueilController@edit');
Route::get('/accueils/pageupdate/{id}','AccueilController@pageupdate');

Route::get('/securites','SecuriteController@index');
Route::get('/securites/list','SecuriteController@list');
Route::post('/securites/create','SecuriteController@create');
Route::get('/securites/read/{id}','SecuriteController@read');
Route::get('/securites/add','SecuriteController@addIntermittant');
Route::get('/securites/delete','SecuriteController@delete');
Route::get('/securites/pagedelete/{id}','SecuriteController@pagedelete');
Route::post('/securites/remove','SecuriteController@remove');
Route::get('/securites/update','SecuriteController@update');
Route::post('/securites/edit','SecuriteController@edit');
Route::get('/securites/pageupdate/{id}','SecuriteController@pageupdate');

Route::get('/hygieenes','HygieeneController@index');
Route::get('/hygieenes/list','HygieeneController@list');
Route::post('/hygieenes/create','HygieeneController@create');
Route::get('/hygieenes/read/{id}','HygieeneController@read');
Route::get('/hygieenes/add','HygieeneController@addIntermittant');
Route::get('/hygieenes/delete','HygieeneController@delete');
Route::get('/hygieenes/pagedelete/{id}','HygieeneController@pagedelete');
Route::post('/hygieenes/remove','HygieeneController@remove');
Route::get('/hygieenes/update','HygieeneController@update');
Route::post('/hygieenes/edit','HygieeneController@edit');
Route::get('/hygieenes/pageupdate/{id}','HygieeneController@pageupdate');

Route::get('/restaurations','RestaurationController@index');
Route::get('/restaurations/list','RestaurationController@list');
Route::post('/restaurations/create','RestaurationController@create');
Route::get('/restaurations/read/{id}','RestaurationController@read');
Route::get('/restaurations/add','RestaurationController@addIntermittant');
Route::get('/restaurations/delete','RestaurationController@delete');
Route::get('/restaurations/pagedelete/{id}','RestaurationController@pagedelete');
Route::post('/restaurations/remove','RestaurationController@remove');
Route::get('/restaurations/update','RestaurationController@update');
Route::post('/restaurations/edit','RestaurationController@edit');
Route::get('/restaurations/pageupdate/{id}','RestaurationController@pageupdate');

Route::get('/sonorisations','SonorisationController@index');
Route::get('/sonorisations/list','SonorisationController@list');
Route::post('/sonorisations/create','SonorisationController@create');
Route::get('/sonorisations/read/{id}','SonorisationController@read');
Route::get('/sonorisations/add','SonorisationController@addIntermittant');
Route::get('/sonorisations/delete','SonorisationController@delete');
Route::get('/sonorisations/pagedelete/{id}','SonorisationController@pagedelete');
Route::post('/sonorisations/remove','SonorisationController@remove');
Route::get('/sonorisations/update','SonorisationController@update');
Route::post('/sonorisations/edit','SonorisationController@edit');
Route::get('/sonorisations/pageupdate/{id}','SonorisationController@pageupdate');

Route::get('/lumieeres','LumieeresController@index');
Route::get('/lumieeres/list','LumieeresController@list');
Route::post('/lumieeres/create','LumieeresController@create');
Route::get('/lumieeres/read/{id}','LumieeresController@read');
Route::get('/lumieeres/add','LumieeresController@addIntermittant');
Route::get('/lumieeres/delete','LumieeresController@delete');
Route::get('/lumieeres/pagedelete/{id}','LumieeresController@pagedelete');
Route::post('/lumieeres/remove','LumieeresController@remove');
Route::get('/lumieeres/update','LumieeresController@update');
Route::post('/lumieeres/edit','LumieeresController@edit');
Route::get('/lumieeres/pageupdate/{id}','LumieeresController@pageupdate');

Route::get('/animations','AnimationController@index');
Route::get('/animations/list','AnimationController@list');
Route::post('/animations/create','AnimationController@create');
Route::get('/animations/read/{id}','AnimationController@read');
Route::get('/animations/add','AnimationController@addIntermittant');
Route::get('/animations/delete','AnimationController@delete');
Route::get('/animations/pagedelete/{id}','AnimationController@pagedelete');
Route::post('/animations/remove','AnimationController@remove');
Route::get('/animations/update','AnimationController@update');
Route::post('/animations/edit','AnimationController@edit');
Route::get('/animations/pageupdate/{id}','AnimationController@pageupdate');

Route::get('/transports','TransportController@index');
Route::get('/transports/list','TransportController@list');
Route::post('/transports/create','TransportController@create');
Route::get('/transports/read/{id}','AnimationController@read');
Route::get('/transports/add','AnimationController@addIntermittant');
Route::get('/transports/delete','AnimationController@delete');
Route::get('/transports/pagedelete/{id}','AnimationController@pagedelete');
Route::post('/transports/remove','AnimationController@remove');
Route::get('/transports/update','AnimationController@update');
Route::post('/transports/edit','AnimationController@edit');
Route::get('/transports/pageupdate/{id}','AnimationController@pageupdate');

Route::get('/humaines','HumainesController@index');
Route::get('/humaines/list','HumainesController@list');
Route::post('/humaines/create','HumainesController@create');
Route::get('/humaines/read/{id}','HumainesController@read');
Route::get('/humaines/add','HumainesController@addHumaine');
Route::get('/humaines/delete','HumainesController@delete');
Route::get('/humaines/pagedelete/{id}','HumainesController@pagedelete');
Route::post('/humaines/remove','HumainesController@remove');
Route::get('/humaines/update','HumainesController@update');
Route::post('/humaines/edit','HumainesController@edit');
Route::get('/humaines/pageupdate/{id}','HumainesController@pageupdate');
Route::get('/humaines/search','HumainesController@search');
Route::post('/humaines/recherche','HumainesController@recherche');

Route::get('/materielles','MateriellesController@index');
Route::get('/materielles/list','MateriellesController@list');
Route::post('/materielles/create','MateriellesController@create');
Route::get('/materielles/read/{id}','MateriellesController@read');
Route::get('/materielles/add','MateriellesController@addMaterielle');
Route::get('/materielles/delete','MateriellesController@delete');
Route::get('/materielles/pagedelete/{id}','MateriellesController@pagedelete');
Route::post('/materielles/remove','MateriellesController@remove');
Route::get('/materielles/update','MateriellesController@update');
Route::post('/materielles/edit','MateriellesController@edit');
Route::get('/materielles/pageupdate/{id}','MateriellesController@pageupdate');
Route::get('/materielles/search','MateriellesController@search');
Route::post('/materielles/recherche','MateriellesController@recherche');
Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
