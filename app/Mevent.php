<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mevent extends Model
{
    protected $fillable = ['title','start_date','end_date'];
}
