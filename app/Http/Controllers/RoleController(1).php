<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
    	$users=DB::table('users')->orderBy('created_at', 'desc')->first();
    	return view('adminlte::auth.role',['users' => $users]);
    }
    public function role(Request $request){
        $role=new Role; 
        $role->id_user=$request->input('user');
        $role->role_user=$request->input('role');
        $role->save();
        return redirect('/home');
    }
}
