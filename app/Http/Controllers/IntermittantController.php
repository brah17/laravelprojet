<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use Illuminate\Support\Facades\DB;

class IntermittantController extends Controller
{
     public function index(){
    	return view('adminlte::homeintermittant');
    }
      public function list(){

      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

      return view('adminlte::listIntermittant',['users' => $users]);
    }
    public function addIntermittant(){
      
      return view("adminlte::ajouterIntermittant");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'name' => 'required',
              'email' => 'required',
              'password' => 'required',
              
            ]);
       
        $name=$request->input('name');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        $password=$request->input('password');
        $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
         $fields = [
            'name'     => $name,
            'email'    => $email,
            'password' => bcrypt($password),
        ];
       $user=new User;
       $user->name=$name;
       $user->email=$email;
       $user->password=$passwordBcrypt;
       $user->save();
       $role=new Role;
       $role->id_user=$user->id;
       $role->role_user="intermittant";
       $role->save();

        

   
    return redirect('/intermittants/list')->with('info','Intermittant ajouté');
        
       



    }
    public function read($id){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($users->all() as $user){
              if($user->id_user==$id){
                $userss=$user;
              }
            }

        
        return view('adminlte::lectureIntermittant',['userss' => $userss]);
    }
    public function delete (){
    $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
        return view('adminlte::supprimerIntermittant',['users' => $users]);
    }
    public function update (){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

        return view('adminlte::modifierIntermittant',['users' => $users]);
    }
    public function pagedelete($id){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($users->all() as $user){
              if($user->id_user==$id){
                $userss=$user;
              }
            }
        return view('adminlte::pagesupprimerIntermittant',['userss' => $userss]);
    }
     public function pageupdate($id){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($users->all() as $user){
              if($user->id_user==$id){
                $userss=$user;
              }
            }
        return view('adminlte::pagemodifierIntermittant',['userss' => $userss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'name' => 'required',
              'password' => 'required',
              

            ]);
       $data=[
          'name' => $request->input('name'),
          'password' => bcrypt($request->input('password')),
         
          
          
       ];
      $id=$request->input('id');
       User::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/intermittants/list')->with('info','Intermittant modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Role::where('id_user',(int)$id)->delete();
               User::where('id',(int)$id)->delete();
               return redirect('/intermittants/list')->with('info','Intermittant supprimeé ');

    }
}
