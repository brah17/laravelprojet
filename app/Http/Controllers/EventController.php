<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Evenemnt;
use App\Lieu;
use App\Mevent;
use Calendar;
use Illuminate\Support\Facades\Input;
class EventController extends Controller
{
    public function index(){
    	$events=Mevent::where('id','>',0)->paginate(5);

    	return view('adminlte::indexEvent',['events' => $events]);
    }
    public function addEvent(){
    	$lieus=Lieu::all();
    	return view("adminlte::ajouterEvent",["lieus" => $lieus]);
    }
    public function create(Request $request){
    	$this->validate(
              $request,[
              'title' => 'required',
              'start_date' => 'required',
              'end_date' => 'required',
              'lieu' => 'required',

            ]);
        $start_date_time = strtotime($request->input('start_date'));
        $end_date_time = strtotime($request->input('end_date'));
        if ($start_date_time > $end_date_time){
          return redirect('/events/ajouter')->with('info','verify start date and the end date');
        }
        $title=$request->input('title');
       // $statut=$request->input('statut');
        $start_date=$request->input('start_date');
        $end_date=$request->input('end_date');
        //$responsable=$request->input('responsable');
        $lieu=$request->input('lieu');
        $id=Auth::id();
        $evenemnt=new Mevent();
        $evenemnt->title=$title;
      
        $evenemnt->start_date=$start_date;
        $evenemnt->end_date=$end_date;
        $evenemnt->user_id=$id;
        //$event->responsable=$responsable;
        $lieu1=Lieu::find((int)$lieu);
        $evenemnt->lieu_id=$lieu1->id;
        $evenemnt->save();
        

   
    return redirect('/events')->with('info','Evenement ajouté');
        
       



    }
    public function read($id){
    	$events=Mevent::find($id);
        
        return view('adminlte::lectureEvent',['events' => $events]);
    }
    public function delete (){
    	$events=Mevent::where('id','>',0)->paginate(5);
        return view('adminlte::supprimerEvent',['events' => $events]);
    }
    public function update (){
    	$events=Mevent::where('id','>',0)->paginate(5);
        return view('adminlte::modifierEvent',['events' => $events]);
    }
    public function pagedelete($id){
    	$events=Mevent::find($id);
        return view('adminlte::pagesupprimerEvent',['events' => $events]);
    }
     public function pageupdate($id){
    	$events=Mevent::find($id);
        return view('adminlte::pagemodifierEvent',['events' => $events]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'title' => 'required',
              'start_date' => 'required',
              'end_date' => 'required',
              'lieus' => 'required',

            ]);
       $data=[
          'title' => $request->input('title'),
          'start_date' => $request->input('start_date'),
          'end_date' => $request->input('end_date'),
          'lieu_id' => $request->input('lieus')
          
          
       ];
      $id=$request->input('id');
       Mevent::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/events')->with('info','Evenement modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Mevent::where('id',(int)$id)->delete();
               return redirect('/events')->with('info','Evenement supprimeé ');

    }
    public function calendar()
    {
        $events = array(
    "2018-04-09 10:30:00" => array(
      "Event 1",
      "Event 2 <strong> with html</stong>",
    ),
    "2018-04-12 14:12:23" => array(
      "Event 3",
    ),
    "2018-05-14 08:00:00" => array(
      "Event 4",
    ),
  );
        $cal = \Skecskes\Calendar\Facades\Calendar::make();
        $cal->setDate(Input::get('cdate')); //Set starting date
  $cal->setBasePath('/dashboard'); // Base path for navigation URLs
  $cal->showNav(true); // Show or hide navigation
  $cal->setView(Input::get('cv')); //'day' or 'week' or null
  $cal->setStartEndHours(8,20); // Set the hour range for day and week view
  $cal->setTimeClass('ctime'); //Class Name for times column on day and week views
  $cal->setEventsWrap(array('<p>', '</p>')); // Set the event's content wrapper
  $cal->setDayWrap(array('<div>','</div>')); //Set the day's number wrapper
  $cal->setNextIcon('>>'); //Can also be html: <i class='fa fa-chevron-right'></i>
  $cal->setPrevIcon('<<'); // Same as above
  $cal->setDayLabels(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat')); //Label names for week days
  $cal->setMonthLabels(array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December')); //Month names
  $cal->setDateWrap(array('<div>','</div>')); //Set cell inner content wrapper
  $cal->setTableClass('table'); //Set the table's class name
  $cal->setHeadClass('table-header'); //Set top header's class name
  $cal->setNextClass('btn'); // Set next btn class name
  $cal->setPrevClass('btn'); // Set Prev btn class name
  $cal->setEvents($events);
        
  return view('adminlte::calendar',['cal' => $cal]);
    }
}
