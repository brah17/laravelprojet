<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Role;
class AdminController extends Controller
{
    public function index(){
    	return view('adminlte::homeadmin');
    }
      public function list(){

      $admins = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

      return view('adminlte::listAdmin',['admins' => $admins]);
    }
    public function addAdmin(){
      
      return view("adminlte::ajouterAdmin");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'name' => 'required',
              'email' => 'required',
              'password' => 'required',
              
            ]);
       
        $name=$request->input('name');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        $password=$request->input('password');
        $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
         $fields = [
            'name'     => $name,
            'email'    => $email,
            'password' => bcrypt($password),
        ];
       $admin=new User;
       $admin->name=$name;
       $admin->email=$email;
       $admin->password=$passwordBcrypt;
       $admin->save();
       $role=new Role;
       $role->id_user=$admin->id;
       $role->role_user="admin";
       $role->save();

        

   
    return redirect('/admins/list')->with('info','Admin ajouté');
        
       



    }
    public function read($id){
      $admins = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($admins->all() as $admin){
              if($admin->id_user==$id){
                $adminss=$admin;
              }
            }

        
        return view('adminlte::lectureAdmin',['adminss' => $adminss]);
    }
    public function delete (){
    $admins = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
        return view('adminlte::supprimerAdmin',['admins' => $admins]);
    }
    public function update (){
      $admins = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

        return view('adminlte::modifierAdmin',['admins' => $admins]);
    }
    public function pagedelete($id){
      $admins = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($admins->all() as $admin){
              if($admin->id_user==$id){
                $adminss=$admin;
              }
            }
        return view('adminlte::pagesupprimerAdmin',['adminss' => $adminss]);
    }
     public function pageupdate($id){
      $admins = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($admins->all() as $admin){
              if($admin->id_user==$id){
                $adminss=$admin;
              }
            }
        return view('adminlte::pagemodifierAdmin',['adminss' => $adminss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'name' => 'required',
              'password' => 'required',
              

            ]);
       $data=[
          'name' => $request->input('name'),
          'password' => bcrypt($request->input('password')),
         
          
          
       ];
      $id=$request->input('id');
       User::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/admins/list')->with('info','Admin modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Role::where('id_user',(int)$id)->delete();
               User::where('id',(int)$id)->delete();
               return redirect('/admins/list')->with('info','Admin supprimeé ');

    }
    public function search(){
      $admins = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
      return view('adminlte::searchAdmins',['admins' => $admins]);
    }
    public function recherche(Request $request){
      $this->validate(
              $request,[
              'search' => 'required',
              
              

            ]);
      $search=$request->input('search');
      $admins = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
    }
}
