<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;


class ProducteurController extends Controller
{
    public function index(){
	 	return view("adminlte::indexProducteur");
	 }
      public function list(){

      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

      return view('adminlte::listProducteur',['users' => $users]);
    }
    public function addProducteur(){
      
      return view("adminlte::ajouterProducteur");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'name' => 'required',
              'email' => 'required',
              'password' => 'required',
              
            ]);
       
        $name=$request->input('name');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        $password=$request->input('password');
        $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
         $fields = [
            'name'     => $name,
            'email'    => $email,
            'password' => bcrypt($password),
        ];
       $user=new User;
       $user->name=$name;
       $user->email=$email;
       $user->password=$passwordBcrypt;
       $user->save();
       $role=new Role;
       $role->id_user=$user->id;
       $role->role_user="producteur";
       $role->save();

        

   
    return redirect('/producteurs/list')->with('info','Producteur ajouté');
        
       



    }
    public function read($id){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($users->all() as $user){
              if($user->id_user==$id){
                $userss=$user;
              }
            }

        
        return view('adminlte::lectureProducteur',['userss' => $userss]);
    }
    public function delete (){
    $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
        return view('adminlte::supprimerProducteur',['users' => $users]);
    }
    public function update (){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

        return view('adminlte::modifierProducteur',['users' => $users]);
    }
    public function pagedelete($id){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($users->all() as $user){
              if($user->id_user==$id){
                $userss=$user;
              }
            }
        return view('adminlte::pagesupprimerProducteur',['userss' => $userss]);
    }
     public function pageupdate($id){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($users->all() as $user){
              if($user->id_user==$id){
                $userss=$user;
              }
            }
        return view('adminlte::pagemodifierProducteur',['userss' => $userss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'name' => 'required',
              'password' => 'required',
              

            ]);
       $data=[
          'name' => $request->input('name'),
          'password' => bcrypt($request->input('password')),
         
          
          
       ];
      $id=$request->input('id');
       User::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/producteurs/list')->with('info','Producteur modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Role::where('id_user',(int)$id)->delete();
               User::where('id',(int)$id)->delete();
               return redirect('/producteurs/list')->with('info','Producteur supprimeé ');

    }
}
