<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logistique;

class LumieeresController extends Controller
{
    public function index(){
	 	return view("adminlte::indexLumieeres");
	 }
     public function list(){

      $logistiques = DB::table('logistiques')
            ->get();

      return view('adminlte::listLumieeres',['logistiques' => $logistiques]);
    }
    public function addLumieeres(){
      
      
      return view("adminlte::ajouterLumieeres");
    }
    public function create(Request $request){
      
       
        $type="lumieeres";
       
       // $statut=$request->input('statut');
       
        //$responsable=$request->input('responsable');
        
        
        
       $logistique=new Logistique;
       $logistique->type=$type;
       
       
       $count = Logistique::where('id', '>=', 0)->count();
       $count=$count+1;
       $logistique->numbill=$count;
       $logistique->save();
       

        

   
    return redirect('/logistiques/list')->with('info','Lumieeres ajouté');
        
       



    }
    public function read($id){
      $logistiques = DB::table('logistiques')
            ->get();
           

        
        return view('adminlte::lectureLumieeres',['logistiques' => $logistiques]);
    }
    public function delete (){
     $logistiques = DB::table('logistiques')
            ->get();
        return view('adminlte::supprimerLumieeres',['logistiques' => $logistiques]);
    }
    public function update (){
      $logistiques = DB::table('logistiques')
            ->get();

        return view('adminlte::modifierLumieeres',['logistiques' => $logistiques]);
    }
    public function pagedelete($id){
       $logistiques = DB::table('logistiques')
            ->get();
            
        return view('adminlte::pagesupprimerLumieeres',['logistiques' => $logistiques]);
    }
     public function pageupdate($id){
       $logistiques = DB::table('logistiques')
            ->get();
            
        return view('adminlte::pagemodifierLumieeres',['logistiques' => $logistiques]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'date_fin' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'date_fin' => $request->input('date_fin'),
          
         
          
          
       ];
      $id=$request->input('id');
       Logistique::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/logistiques/list')->with('info','Lumieeres modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Logistique::where('id',(int)$id)->delete();
               return redirect('/logistiques/list')->with('info','Lumieeres supprimeé ');

    }
}
