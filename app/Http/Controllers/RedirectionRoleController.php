<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Role;
class RedirectionRoleController extends Controller
{
    public function role(){
    	$id=Auth::id();
    	$role = Role::where('id_user', $id)->first();
    	
    	if($role!=null){
            if($role->role_user=='admin'){
            	return redirect('/admin');
            }
            else{
            	return redirect('/home');
            }
    	}
    	return redirect('/register');
    	
    }
}
