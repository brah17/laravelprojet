<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logistique;
class RestaurationController extends Controller
{
     public function index(){
	 	return view("adminlte::indexRestauration");
	 }
     public function list(){

      $logistiques = DB::table('logistiques')
            ->get();

      return view('adminlte::listRestauration',['logistiques' => $logistiques]);
    }
    public function addRestauration(){
      
      
      return view("adminlte::ajouterRestauration");
    }
    public function create(Request $request){
      
       
        $type="restauration";
       
       // $statut=$request->input('statut');
       
        //$responsable=$request->input('responsable');
        
        
        
       $logistique=new Logistique;
       $logistique->type=$type;
       
       
       $count = Logistique::where('id', '>=', 0)->count();
       $count=$count+1;
       $logistique->numbill=$count;
       $logistique->save();
       

        

   
    return redirect('/logistiques/list')->with('info','Restauration ajouté');
        
       



    }
    public function read($id){
      $logistiques = DB::table('logistiques')
            ->get();
           

        
        return view('adminlte::lectureRestauration',['logistiques' => $logistiques]);
    }
    public function delete (){
     $logistiques = DB::table('logistiques')
            ->get();
        return view('adminlte::supprimerRestauration',['logistiques' => $logistiques]);
    }
    public function update (){
      $logistiques = DB::table('logistiques')
            ->get();

        return view('adminlte::modifierRestauration',['logistiques' => $logistiques]);
    }
    public function pagedelete($id){
       $logistiques = DB::table('logistiques')
            ->get();
            
        return view('adminlte::pagesupprimerRestauration',['logistiques' => $logistiques]);
    }
     public function pageupdate($id){
       $logistiques = DB::table('logistiques')
            ->get();
            
        return view('adminlte::pagemodifierRestauration',['logistiques' => $logistiques]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'date_fin' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'date_fin' => $request->input('date_fin'),
          
         
          
          
       ];
      $id=$request->input('id');
       Logistique::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/logistiques/list')->with('info','Restauration modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Logistique::where('id',(int)$id)->delete();
               return redirect('/logistiques/list')->with('info','Restauration supprimeé ');

    }
}
