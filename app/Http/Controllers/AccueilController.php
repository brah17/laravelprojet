<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logistique;
class AccueilController extends Controller
{
     public function index(){
	 	return view("adminlte::indexAccueil");
	 }
     public function list(){

      $resources = DB::table('resources')
            ->get();

      return view('adminlte::listAccueil',['resources' => $resources]);
    }
    public function addAccueil(){
      
      
      return view("adminlte::ajouterAccueil");
    }
    public function create(Request $request){
      
       
        $type="accueil";
       
       // $statut=$request->input('statut');
       
        //$responsable=$request->input('responsable');
        
        
        
       $resource=new Logistique;
       $resource->type=$type;
       
       
       $count = Logistique::where('id', '>=', 0)->count();
       $count=$count+1;
       $resource->numbill=$count;
       $resource->save();
       

        

   
    return redirect('/resources/list')->with('info','Accueil ajouté');
        
       



    }
    public function read($id){
      $resources = DB::table('resources')
            ->get();
           

        
        return view('adminlte::lectureAccueil',['resources' => $resources]);
    }
    public function delete (){
     $resources = DB::table('resources')
            ->get();
        return view('adminlte::supprimerAccueil',['resources' => $resources]);
    }
    public function update (){
      $resources = DB::table('resources')
            ->get();

        return view('adminlte::modifierAccueil',['resources' => $resources]);
    }
    public function pagedelete($id){
       $resources = DB::table('resources')
            ->get();
            
        return view('adminlte::pagesupprimerAccueil',['resources' => $resources]);
    }
     public function pageupdate($id){
       $resources = DB::table('resources')
            ->get();
            
        return view('adminlte::pagemodifierAccueil',['resources' => $resources]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'date_fin' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'date_fin' => $request->input('date_fin'),
          
         
          
          
       ];
      $id=$request->input('id');
       Logistique::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/resources/list')->with('info','Accueil modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Logistique::where('id',(int)$id)->delete();
               return redirect('/resources/list')->with('info','Accueil supprimeé ');

    }
}
