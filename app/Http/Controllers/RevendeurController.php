<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\DB;

class RevendeurController extends Controller
{
     public function index(){
	 	return view("adminlte::indexClient");
	 }
      public function list(){

      $revendeurs = DB::table('clients')
            ->get();

      return view('adminlte::listRevendeur',['revendeurs' => $revendeurs]);
    }
    public function addRevendeur(){
      
      return view("adminlte::ajouterRevendeur");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'email' => 'required|email|max:60|unique:clients',
              'pays' => 'required',
              
            ]);
       
        $nom=$request->input('nom');
        $prenom=$request->input('prenom');
        $pays=$request->input('pays');
        $tel=$request->input('tel');
       // $statut=$request->input('statut');
        $email=$request->input('email');
       // $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
        
       $client=new Client;
       $client->nom=$nom;
       $client->prenom=$prenom;
       $client->email=$email;
       $client->tel=$tel;
       $client->pays=$pays;
       $client->typeclient="revendeur";
       $client->save();
       

        

   
    return redirect('/revendeurs/list')->with('info','Revendeur ajouté');
        
       



    }
    public function read($id){
     $revendeur = Client::where('id', $id)->first();
           

        
        return view('adminlte::lectureRevendeur',['revendeur' => $revendeur]);
    }
    public function delete (){
    $revendeurs = Client::where('typeclient','revendeur')->get();
        return view('adminlte::supprimerRevendeur',['revendeurs' => $revendeurs]);
    }
    public function update (){
         $revendeurs = Client::where('typeclient','revendeur')->get();

        return view('adminlte::modifierRevendeur',['revendeurs' => $revendeurs]);
    }
    public function pagedelete($id){
       $revendeur = Client::where('id', $id)->first();
        return view('adminlte::pagesupprimerRevendeur',['revendeur' => $revendeur]);
    }
     public function pageupdate($id){
            $revendeur = Client::where('id', $id)->first();

        return view('adminlte::pagemodifierRevendeur',['revendeur' => $revendeur]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'tel' => 'required',
           
              

            ]);
       $data=[
          'nom' => $request->input('nom'),
          'prenom' => $request->input('prenom'),
          'tel' => $request->input('tel'),
         
          
          
       ];
      $id=$request->input('id');
       Client::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/revendeurs/list')->with('info','Revendeur modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Client::where('id',(int)$id)->delete();
               return redirect('/revendeurs/list')->with('info','Revendeur supprimeé ');

    }
}
