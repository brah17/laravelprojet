<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HygieeneController extends Controller
{
    
     public function index(){
	 	return view("adminlte::indexHygieene");
	 }
     public function list(){

      $logistiques = DB::table('logistiques')
            ->get();

      return view('adminlte::listHygieene',['logistiques' => $logistiques]);
    }
    public function addHygieene(){
      
      
      return view("adminlte::ajouterHygieene");
    }
    public function create(Request $request){
      
       
        $type="securite";
       
       // $statut=$request->input('statut');
       
        //$responsable=$request->input('responsable');
        
        
        
       $logistique=new Logistique;
       $logistique->type=$type;
       
       
       $count = Logistique::where('id', '>=', 0)->count();
       $count=$count+1;
       $logistique->numbill=$count;
       $logistique->save();
       

        

   
    return redirect('/logistiques/list')->with('info','Hygieene ajouté');
        
       



    }
    public function read($id){
      $logistiques = DB::table('logistiques')
            ->get();
           

        
        return view('adminlte::lectureHygieene',['logistiques' => $logistiques]);
    }
    public function delete (){
     $logistiques = DB::table('logistiques')
            ->get();
        return view('adminlte::supprimerHygieene',['logistiques' => $logistiques]);
    }
    public function update (){
      $logistiques = DB::table('logistiques')
            ->get();

        return view('adminlte::modifierHygieene',['logistiques' => $logistiques]);
    }
    public function pagedelete($id){
       $logistiques = DB::table('logistiques')
            ->get();
            
        return view('adminlte::pagesupprimerHygieene',['logistiques' => $logistiques]);
    }
     public function pageupdate($id){
       $logistiques = DB::table('logistiques')
            ->get();
            
        return view('adminlte::pagemodifierHygieene',['logistiques' => $logistiques]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'date_fin' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'date_fin' => $request->input('date_fin'),
          
         
          
          
       ];
      $id=$request->input('id');
       Logistique::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/logistiques/list')->with('info','Hygieene modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Logistique::where('id',(int)$id)->delete();
               return redirect('/logistiques/list')->with('info','Hygieene supprimeé ');

    }
}
