<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Carteacess;

class CarteAcessController extends Controller
{
    public function index(){
	 	return view("adminlte::indexCarteAcess");
	 }
     public function list(){

      $carteacesss = DB::table('carteacesss')
            ->get();

      return view('adminlte::listCarteAcess',['carteacesss' => $carteacesss]);
    }
    public function addCarteAcess(){
      
      $userss=User::all();
      return view("adminlte::ajouterCarteAcess",["users"=>$users]);
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'user' => 'required',
            ]);
       
        $user=$request->input('user');
        $role=Role::find((int)$user);
       // $statut=$request->input('statut');
       
        //$responsable=$request->input('responsable');
        
        
        
       $carteacess=new Carteacess;
       $carteacess->iduser=(int)$client;
       $carteacess->role=$role->role_user;
       
       
       $count = Carteacess::where('id', '>=', 0)->count();
       $count=$count+1;
       $carteacess->numbill=$count;
       $carteacess->save();
       

        

   
    return redirect('/carteacesss/list')->with('info','CarteAcess ajouté');
        
       



    }
    public function read($id){
      $carteacesss = DB::table('carteacesss')
            ->get();
           

        
        return view('adminlte::lectureCarteAcess',['carteacesss' => $carteacesss]);
    }
    public function delete (){
     $carteacesss = DB::table('carteacesss')
            ->get();
        return view('adminlte::supprimerCarteAcess',['carteacesss' => $carteacesss]);
    }
    public function update (){
      $carteacesss = DB::table('carteacesss')
            ->get();

        return view('adminlte::modifierCarteAcess',['carteacesss' => $carteacesss]);
    }
    public function pagedelete($id){
       $carteacesss = DB::table('carteacesss')
            ->get();
            
        return view('adminlte::pagesupprimerCarteAcess',['carteacesss' => $carteacesss]);
    }
     public function pageupdate($id){
       $carteacesss = DB::table('carteacesss')
            ->get();
            
        return view('adminlte::pagemodifierCarteAcess',['carteacesss' => $carteacesss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'date_fin' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'date_fin' => $request->input('date_fin'),
          
         
          
          
       ];
      $id=$request->input('id');
       Carteacess::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/carteacesss/list')->with('info','CarteAcess modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Carteacess::where('id',(int)$id)->delete();
               return redirect('/carteacesss/list')->with('info','CarteAcess supprimeé ');

    }
}
