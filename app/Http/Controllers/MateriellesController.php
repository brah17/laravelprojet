<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use Illuminate\Support\Facades\DB;

class MateriellesController extends Controller
{
     public function index(){
	 	return view("adminlte::indexMaterielles");
	 }
     public function list(){

      $materielles = DB::table('resources')
            ->get();

      return view('adminlte::listMaterielle',['materielles' => $materielles]);
    }
    public function addMaterielle(){
      
      
      return view("adminlte::ajouterMaterielle");
    }
    public function create(Request $request){
      
       
        $type="materielle";
       
       // $statut=$request->input('statut');
       
        //$responsable=$request->input('responsable');
        
        
        
       $resource=new Resource;
       $resource->type=$type;
       
       
       $count = Resource::where('id', '>=', 0)->count();
       $count=$count+1;
       $resource->num=$count;
       $resource->save();
       

        

   
    return redirect('/materielles/list')->with('info','Materielle ajouté');
        
       



    }
    public function read($id){
      
           
        $materielle=Resource::find($id);
        
        return view('adminlte::lectureMaterielle',['materielle' => $materielle]);
    }
    public function delete (){
      $materielles = DB::table('resources')
            ->get();
        return view('adminlte::supprimerMaterielle',['materielles' => $materielles]);
    }
    public function update (){
      $materielles = DB::table('resources')
            ->get();

        return view('adminlte::modifierMaterielle',['materielles' => $materielles]);
    }
    public function pagedelete($id){
        $materielle=Resource::find($id);
            
        return view('adminlte::pagesupprimerMaterielle',['materielle' => $materielle]);
    }
     public function pageupdate($id){
       $resources = DB::table('resources')
            ->get();
            
        return view('adminlte::pagemodifierMaterielle',['resources' => $resources]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'date_fin' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'date_fin' => $request->input('date_fin'),
          
         
          
          
       ];
      $id=$request->input('id');
       Resource::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/materielles/list')->with('info','Materielle modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Resource::where('id',(int)$id)->delete();
               return redirect('/materielles/list')->with('info','Materielle supprimeé ');

    }
}
