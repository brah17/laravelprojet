<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SonorisationController extends Controller
{
     public function index(){
	 	return view("adminlte::indexSonorisation");
	 }
     public function list(){

      $logistiques = DB::table('logistiques')
            ->get();

      return view('adminlte::listSonorisation',['logistiques' => $logistiques]);
    }
    public function addSonorisation(){
      
      
      return view("adminlte::ajouterSonorisation");
    }
    public function create(Request $request){
      
       
        $type="sonorisation";
       
       // $statut=$request->input('statut');
       
        //$responsable=$request->input('responsable');
        
        
        
       $logistique=new Logistique;
       $logistique->type=$type;
       
       
       $count = Logistique::where('id', '>=', 0)->count();
       $count=$count+1;
       $logistique->numbill=$count;
       $logistique->save();
       

        

   
    return redirect('/logistiques/list')->with('info','Sonorisation ajouté');
        
       



    }
    public function read($id){
      $logistiques = DB::table('logistiques')
            ->get();
           

        
        return view('adminlte::lectureSonorisation',['logistiques' => $logistiques]);
    }
    public function delete (){
     $logistiques = DB::table('logistiques')
            ->get();
        return view('adminlte::supprimerSonorisation',['logistiques' => $logistiques]);
    }
    public function update (){
      $logistiques = DB::table('logistiques')
            ->get();

        return view('adminlte::modifierSonorisation',['logistiques' => $logistiques]);
    }
    public function pagedelete($id){
       $logistiques = DB::table('logistiques')
            ->get();
            
        return view('adminlte::pagesupprimerSonorisation',['logistiques' => $logistiques]);
    }
     public function pageupdate($id){
       $logistiques = DB::table('logistiques')
            ->get();
            
        return view('adminlte::pagemodifierSonorisation',['logistiques' => $logistiques]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'date_fin' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'date_fin' => $request->input('date_fin'),
          
         
          
          
       ];
      $id=$request->input('id');
       Logistique::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/logistiques/list')->with('info','Sonorisation modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Logistique::where('id',(int)$id)->delete();
               return redirect('/logistiques/list')->with('info','Sonorisation supprimeé ');

    }
}
