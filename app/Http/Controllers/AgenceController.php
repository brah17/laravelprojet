<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\DB;
class AgenceController extends Controller
{
    public function index(){
	 	return view("adminlte::indexClient");
	 }
      public function list(){

      $agences = DB::table('clients')
            ->get();

      return view('adminlte::listAgence',['agences' => $agences]);
    }
    public function addAgence(){
      
      return view("adminlte::ajouterAgence");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'email' => 'required|email|max:60|unique:clients',
              'pays' => 'required',
              'tel' => 'required',
              
            ]);
       
        $nom=$request->input('nom');
        $prenom=$request->input('prenom');
        $pays=$request->input('pays');
        $tel=$request->input('tel');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        
       // $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
        
       $client=new Client;
       $client->nom=$nom;
       $client->prenom=$prenom;
       $client->email=$email;
       $client->tel=$tel;
       $client->pays=$pays;
       $client->typeclient="agence";
       $client->save();
       

        

   
    return redirect('/agences/list')->with('info','Agence ajouté');
        
       



    }
    public function read($id){
     $agence = Client::where('id', $id)->first();
           

        
        return view('adminlte::lectureAgence',['agence' => $agence]);
    }
    public function delete (){
    $agences = Client::where('typeclient','agence')->get();
        return view('adminlte::supprimerAgence',['agences' => $agences]);
    }
    public function update (){
         $agences = Client::where('typeclient','agence')->get();

        return view('adminlte::modifierAgence',['agences' => $agences]);
    }
    public function pagedelete($id){
        $agence = Client::where('id', $id)->first();
        return view('adminlte::pagesupprimerAgence',['agence' => $agence]);
    }
     public function pageupdate($id){
            $agence = Client::where('id', $id)->first();

        return view('adminlte::pagemodifierAgence',['agence' => $agence]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'tel' => 'required',
            
              

            ]);
       $data=[
          'nom' => $request->input('nom'),
          'prenom' => $request->input('prenom'),
          'tel' => $request->input('tel'),
      
         
          
          
       ];
      $id=$request->input('id');
       Client::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/agences/list')->with('info','Agence modifieé');
    }
     public function remove(Request $request){
              $id=$request->input('id');
               Client::where('id',(int)$id)->delete();
               return redirect('/agences/list')->with('info','Agence supprimeé ');

    }
}
