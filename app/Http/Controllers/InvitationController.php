<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Client;
use App\Mevent;
class InvitationController extends Controller
{
    public function index(){
	 	return view("adminlte::indexInvitation");
	 }
     public function list(){

      $invitations = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();

      return view('adminlte::listInvitation',['tickets' => $invitations]);
    }
    public function addInvitation(){
      $mevents=Mevent::all();
      $clients=Client::all();
      return view("adminlte::ajouterInvitation",["mevents" => $mevents,"clients"=>$clients]);
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'client' => 'required',
              'event' => 'required';
              'prix' => 'required',
              
            ]);
       
        $client=$request->input('client');
       // $statut=$request->input('statut');
        $event=$request->input('event');
        $prix=$request->input('prix');
        //$responsable=$request->input('responsable');
        
        
        
       $invitation=new Ticket;
       $invitation->idclient=(int)$client;
       $invitation->idevent=(int)$event;
       $invitation->prix=$prix
       $invitation->type="invitation";
       $count = Ticket::where('type', '=', 'invitation')->count();
       $count=$count+1;
       $invitation->numbill=$count;
       $invitation->save();
       

        

   
    return redirect('/invitations/list')->with('info','Invitation ajouté');
        
       



    }
    public function read($id){
      $invitations = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();
            foreach($invitations->all() as $invitation){
              if($invitation->id==$id){
                $invitationss=$invitation;
              }
            }

        
        return view('adminlte::lectureInvitation',['invitationss' => $invitationss]);
    }
    public function delete (){
     $invitations = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();
        return view('adminlte::supprimerInvitation',['invitations' => $invitations]);
    }
    public function update (){
      $invitations = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();

        return view('adminlte::modifierInvitation',['invitations' => $invitations]);
    }
    public function pagedelete($id){
       $invitations = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();
            foreach($invitations->all() as $invitation){
              if($invitation->id_user==$id){
                $invitationss=$invitation;
              }
            }
        return view('adminlte::pagesupprimerInvitation',['invitationss' => $invitationss]);
    }
     public function pageupdate($id){
       $invitations = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();
            foreach($invitations->all() as $invitation){
              if($invitation->id==$id){
                $invitationss=$invitation;
              }
            }
        return view('adminlte::pagemodifierInvitation',['invitationss' => $invitationss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'client' => 'required',
              'event' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'client' => $request->input('client'),
          'event' => $request->input('event'),
         
          
          
       ];
      $id=$request->input('id');
       Ticket::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/invitations/list')->with('info','Invitation modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Ticket::where('id',(int)$id)->delete();
               return redirect('/invitations/list')->with('info','Invitation supprimeé ');

    }
}
