<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Abonement;

class AbonnementController extends Controller
{
    public function index(){
	 	return view("adminlte::indexAbonement");
	 }
     public function list(){

      $abonements = DB::table('abonements')
            ->leftJoin('clients', 'abonements.id', '=', 'clients.idclient')
            ->leftJoin('mevents','abonements.id','=','clients.idevent')
            ->get();

      return view('adminlte::listAbonement',['abonements' => $abonements]);
    }
    public function addAbonement(){
      $mevents=Mevent::all();
      $clients=Client::all();
      return view("adminlte::ajouterAbonement",["mevents" => $mevents,"clients"=>$clients]);
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'client' => 'required',
              'event' => 'required';
              'prix' => 'required',
              
            ]);
       
        $client=$request->input('client');
       // $statut=$request->input('statut');
        $date_debut=$request->input('date_debut');
        $date_fin=$request->input('date_fin');
        $prix=$request->input('prix'),
        //$responsable=$request->input('responsable');
        
        
        
       $abonement=new Abonement;
       $abonement->idclient=(int)$client;
       $abonement->date_debut=$date_debut;
       $abonement->date_fin=$date_fin
       $abonement->prix=$prix
       
       $count = Abonement::where('id', '>=', 0)->count();
       $count=$count+1;
       $abonement->numbill=$count;
       $abonement->save();
       

        

   
    return redirect('/abonements/list')->with('info','Abonement ajouté');
        
       



    }
    public function read($id){
      $abonements = DB::table('abonements')
            ->get();
           

        
        return view('adminlte::lectureAbonement',['abonements' => $abonements]);
    }
    public function delete (){
     $abonements = DB::table('abonements')
            ->get();
        return view('adminlte::supprimerAbonement',['abonements' => $abonements]);
    }
    public function update (){
      $abonements = DB::table('abonements')
            ->get();

        return view('adminlte::modifierAbonement',['abonements' => $abonements]);
    }
    public function pagedelete($id){
       $abonements = DB::table('abonements')
            ->get();
            
        return view('adminlte::pagesupprimerAbonement',['abonements' => $abonements]);
    }
     public function pageupdate($id){
       $abonements = DB::table('abonements')
            ->leftJoin('clients', 'abonements.id', '=', 'clients.idclient')
            ->leftJoin('mevents','abonements.id','=','clients.idevent')
            ->get();
            foreach($abonements->all() as $abonement){
              if($abonement->id==$id){
                $abonements=$abonement;
              }
            }
        return view('adminlte::pagemodifierAbonement',['abonements' => $abonements]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'date_fin' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'date_fin' => $request->input('date_fin'),
          
         
          
          
       ];
      $id=$request->input('id');
       Abonement::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/abonements/list')->with('info','Abonement modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Abonement::where('id',(int)$id)->delete();
               return redirect('/abonements/list')->with('info','Abonement supprimeé ');

    }
}
