<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Client;
use App\Mevent;
class TicketController extends Controller
{
    public function index(){
	 	return view("adminlte::indexTicket");
	 }
     public function list(){

      $tickets = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();

      return view('adminlte::listTicket',['tickets' => $tickets]);
    }
    public function addTicket(){
      $mevents=Mevent::all();
      $clients=Client::all();
      return view("adminlte::ajouterTicket",["mevents" => $mevents,"clients"=>$clients]);
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'client' => 'required',
              'event' => 'required';
              'prix' => 'required',
              
            ]);
       
        $client=$request->input('client');
       // $statut=$request->input('statut');
        $event=$request->input('event');
        $prix=$request->input('prix');
        //$responsable=$request->input('responsable');
        
        
        
       $ticket=new Ticket;
       $ticket->idclient=(int)$client;
       $ticket->idevent=(int)$event;
       $ticket->prix=$prix
       $ticket->type="ticket";
       $count = Ticket::where('type', '=', 'ticket')->count();
       $count=$count+1;
       $ticket->numbill=$count;
       $ticket->save();
       

        

   
    return redirect('/tickets/list')->with('info','Ticket ajouté');
        
       



    }
    public function read($id){
      $tickets = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();
            foreach($tickets->all() as $ticket){
              if($ticket->id==$id){
                $ticketss=$ticket;
              }
            }

        
        return view('adminlte::lectureTicket',['ticketss' => $ticketss]);
    }
    public function delete (){
     $tickets = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();
        return view('adminlte::supprimerTicket',['tickets' => $tickets]);
    }
    public function update (){
      $tickets = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();

        return view('adminlte::modifierTicket',['tickets' => $tickets]);
    }
    public function pagedelete($id){
       $tickets = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();
            foreach($tickets->all() as $ticket){
              if($ticket->id_user==$id){
                $ticketss=$ticket;
              }
            }
        return view('adminlte::pagesupprimerTicket',['ticketss' => $ticketss]);
    }
     public function pageupdate($id){
       $tickets = DB::table('tickets')
            ->leftJoin('clients', 'tickets.id', '=', 'clients.idclient')
            ->leftJoin('mevents','tickets.id','=','clients.idevent')
            ->get();
            foreach($tickets->all() as $ticket){
              if($ticket->id==$id){
                $ticketss=$ticket;
              }
            }
        return view('adminlte::pagemodifierTicket',['ticketss' => $ticketss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'client' => 'required',
              'event' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'client' => $request->input('client'),
          'event' => $request->input('event'),
         
          
          
       ];
      $id=$request->input('id');
       Ticket::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/tickets/list')->with('info','Ticket modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Ticket::where('id',(int)$id)->delete();
               return redirect('/tickets/list')->with('info','Ticket supprimeé ');

    }
}
