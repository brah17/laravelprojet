<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Role;
class SuperviseurController extends Controller
{
     public function index(){
	 	return view("adminlte::indexSuperviseur");
	 }
       public function list(){

      $superviseurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

      return view('adminlte::listSuperviseur',['superviseurs' => $superviseurs]);
    }
    public function addSuperviseur(){
      
      return view("adminlte::ajouterSuperviseur");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'name' => 'required',
              'email' => 'required',
              'password' => 'required',
              
            ]);
       
        $name=$request->input('name');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        $password=$request->input('password');
        $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
         $fields = [
            'name'     => $name,
            'email'    => $email,
            'password' => bcrypt($password),
        ];
       $superviseur=new User;
       $superviseur->name=$name;
       $superviseur->email=$email;
       $superviseur->password=$passwordBcrypt;
       $superviseur->save();
       $role=new Role;
       $role->id_user=$superviseur->id;
       $role->role_user="superviseur";
       $role->save();

        

   
    return redirect('/superviseurs/list')->with('info','Superviseur ajouté');
        
       



    }
    public function read($id){
      $superviseurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($superviseurs->all() as $superviseur){
              if($superviseur->id_user==$id){
                $superviseurss=$superviseur;
              }
            }

        
        return view('adminlte::lectureSuperviseur',['superviseurss' => $superviseurss]);
    }
    public function delete (){
    $superviseurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
        return view('adminlte::supprimerSuperviseur',['superviseurs' => $superviseurs]);
    }
    public function update (){
      $superviseurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

        return view('adminlte::modifierSuperviseur',['superviseurs' => $superviseurs]);
    }
    public function pagedelete($id){
      $superviseurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($superviseurs->all() as $superviseur){
              if($superviseur->id_user==$id){
                $superviseurss=$superviseur;
              }
            }
        return view('adminlte::pagesupprimerSuperviseur',['superviseurss' => $superviseurss]);
    }
     public function pageupdate($id){
      $superviseurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($superviseurs->all() as $superviseur){
              if($superviseur->id_user==$id){
                $superviseurss=$superviseur;
              }
            }
        return view('adminlte::pagemodifierSuperviseur',['superviseurss' => $superviseurss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'name' => 'required',
              'password' => 'required',
              

            ]);
       $data=[
          'name' => $request->input('name'),
          'password' => bcrypt($request->input('password')),
         
          
          
       ];
      $id=$request->input('id');
       User::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/superviseurs/list')->with('info','Superviseur modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Role::where('id_user',(int)$id)->delete();
               User::where('id',(int)$id)->delete();
               return redirect('/superviseurs/list')->with('info','Superviseur supprimeé ');

    }
}
