<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Role;
class ControleurController extends Controller
{
     public function index(){
	 	return view("adminlte::indexControleur");
	 }
     
       public function list(){

      $controleurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

      return view('adminlte::listControleur',['controleurs' => $controleurs]);
    }
    public function addControleur(){
      
      return view("adminlte::ajouterControleur");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'name' => 'required',
              'email' => 'required',
              'password' => 'required',
              
            ]);
       
        $name=$request->input('name');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        $password=$request->input('password');
        $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
         $fields = [
            'name'     => $name,
            'email'    => $email,
            'password' => bcrypt($password),
        ];
       $controleur=new User;
       $controleur->name=$name;
       $controleur->email=$email;
       $controleur->password=$passwordBcrypt;
       $controleur->save();
       $role=new Role;
       $role->id_user=$controleur->id;
       $role->role_user="controleur";
       $role->save();

        

   
    return redirect('/controleurs/list')->with('info','Controleur ajouté');
        
       



    }
    public function read($id){
      $controleurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($controleurs->all() as $controleur){
              if($controleur->id_user==$id){
                $controleurss=$controleur;
              }
            }

        
        return view('adminlte::lectureControleur',['controleurss' => $controleurss]);
    }
    public function delete (){
    $controleurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
        return view('adminlte::supprimerControleur',['controleurs' => $controleurs]);
    }
    public function update (){
      $controleurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

        return view('adminlte::modifierControleur',['controleurs' => $controleurs]);
    }
    public function pagedelete($id){
      $controleurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($controleurs->all() as $controleur){
              if($controleur->id_user==$id){
                $controleurss=$controleur;
              }
            }
        return view('adminlte::pagesupprimerControleur',['controleurss' => $controleurss]);
    }
     public function pageupdate($id){
      $controleurs = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($controleurs->all() as $controleur){
              if($controleur->id_user==$id){
                $controleurss=$controleur;
              }
            }
        return view('adminlte::pagemodifierControleur',['controleurss' => $controleurss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'name' => 'required',
              'password' => 'required',
              

            ]);
       $data=[
          'name' => $request->input('name'),
          'password' => bcrypt($request->input('password')),
         
          
          
       ];
      $id=$request->input('id');
       User::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/controleurs/list')->with('info','Controleur modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Role::where('id_user',(int)$id)->delete();
               User::where('id',(int)$id)->delete();
               return redirect('/controleurs/list')->with('info','Controleur supprimeé ');

    }
}
