<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Role;
use App\User;
class MandataireController extends Controller
{
	 public function index(){
	 	return view("adminlte::indexMandataire");
	 }
     public function list(){

      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

      return view('adminlte::listMandataire',['users' => $users]);
    }
    public function addMandataire(){
      
      return view("adminlte::ajouterMandataire");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'name' => 'required',
              'email' => 'required',
              'password' => 'required',
              
            ]);
       
        $name=$request->input('name');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        $password=$request->input('password');
        $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
         $fields = [
            'name'     => $name,
            'email'    => $email,
            'password' => bcrypt($password),
        ];
       $user=new User;
       $user->name=$name;
       $user->email=$email;
       $user->password=$passwordBcrypt;
       $user->save();
       $role=new Role;
       $role->id_user=$user->id;
       $role->role_user="mandataire";
       $role->save();

        

   
    return redirect('/mandataires/list')->with('info','Mandataire ajouté');
        
       



    }
    public function read($id){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($users->all() as $user){
              if($user->id==$id){
                $userss=$user;
              }
            }

        
        return view('adminlte::lectureMandataire',['userss' => $userss]);
    }
    public function delete (){
    $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
        return view('adminlte::supprimerMandataire',['users' => $users]);
    }
    public function update (){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

        return view('adminlte::modifierMandataire',['users' => $users]);
    }
    public function pagedelete($id){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($users->all() as $user){
              if($user->id_user==$id){
                $userss=$user;
              }
            }
        return view('adminlte::pagesupprimerMandataire',['userss' => $userss]);
    }
     public function pageupdate($id){
      $users = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($users->all() as $user){
              if($user->id==$id){
                $userss=$user;
              }
            }
        return view('adminlte::pagemodifierMandataire',['userss' => $userss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'name' => 'required',
              'password' => 'required',
              

            ]);
       $data=[
          'name' => $request->input('name'),
          'password' => bcrypt($request->input('password')),
         
          
          
       ];
      $id=$request->input('id');
       User::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/mandataires/list')->with('info','Mandataire modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Role::where('id_user',(int)$id)->delete();
               User::where('id',(int)$id)->delete();
               return redirect('/mandataires/list')->with('info','Mandataire supprimeé ');

    }
}
