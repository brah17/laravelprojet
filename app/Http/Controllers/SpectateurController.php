<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\DB;
class SpectateurController extends Controller
{
    public function index(){
	 	return view("adminlte::indexClient");
	 }
      public function list(){

      $spectateurs = DB::table('clients')
            ->get();

      return view('adminlte::spectateurList',['spectateurs' => $spectateurs]);
    }
    public function addSpectateur(){
      
      return view("adminlte::ajouterSpectateur");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'email' => 'required|email|max:60|unique:clients',
              'pays' => 'required',
              
            ]);
       
        $nom=$request->input('nom');
        $prenom=$request->input('prenom');
        $pays=$request->input('pays');
        $tel=$request->input('tel');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        $password=bcrypt($request->input('password'));
       // $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
        
       $client=new Client;
       $client->nom=$nom;
       $client->prenom=$prenom;
       $client->email=$email;
       $client->tel=$tel;
       $client->pays=$pays;
       $client->typeclient="spectateur";
       $client->save();
       

        

   
    return redirect('/spectateurs/list')->with('info','Spectateur ajouté');
        
       



    }
    public function read($id){
     $spectateur = Client::where('id', $id)->first();
           

        
        return view('adminlte::lectureSpectateur',['spectateur' => $spectateur]);
    }
    public function delete (){
    $spectateurs = Client::where('typeclient','Spectateur')->get();
        return view('adminlte::supprimerSpectateur',['spectateurs' => $spectateurs]);
    }
    public function update (){
         $spectateurs = Client::where('typeclient','spectateur')->get();

        return view('adminlte::modifierSpectateur',['spectateurs' => $spectateurs]);
    }
    public function pagedelete($id){
       $spectateur = Client::where('id', $id)->first();
        return view('adminlte::pagesupprimerSpectateur',['spectateur' => $spectateur]);
    }
     public function pageupdate($id){
            $spectateur = Client::where('id', $id)->first();

        return view('adminlte::pagemodifierSpectateur',['spectateur' => $spectateur]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'tel' => 'required',
              
              

            ]);
       $data=[
          'nom' => $request->input('nom'),
          'prenom' => $request->input('prenom'),
          'tel' => $request->input('tel'),
          
         
          
          
       ];
      $id=$request->input('id');
       Client::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/spectateurs/list')->with('info','spectateur modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Client::where('id',(int)$id)->delete();
               return redirect('/spectateurs/list')->with('info','spectateur supprimeé ');

    }
}
