<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\DB;
class DestributeurController extends Controller
{
    public function index(){
	 	return view("adminlte::indexClient");
	 }
      public function list(){

      $uses = DB::table('clients')
            ->get();

      return view('adminlte::listDestributeur',['destributeurs' => $destributeurs]);
    }
    public function addDestributeur(){
      
      return view("adminlte::ajouterDestributeur");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'email' => 'required|email|max:60|unique:clients',
              'pays' => 'required',
              'password' => 'required',
              
            ]);
       
        $nom=$request->input('nom');
        $prenom=$request->input('prenom');
        $pays=$request->input('pays');
        $tel=$request->input('tel');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        $password=bcrypt($request->input('password'));
       // $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
        
       $client=new Client;
       $client->nom=$nom;
       $client->prenom=$prenom
       $client->email=$email;
       $client->password=$password;
       $client->tel=$tel;
       $client->pays=$pays;
       $client->typeclient="destributeur";
       $client->save();
       

        

   
    return redirect('/Destributeurs/list')->with('info','Destributeur ajouté');
        
       



    }
    public function read($id){
     $destributeur = Client::where('id', $id)->first();
           

        
        return view('adminlte::lectureDestributeur',['destributeur' => $destributeur]);
    }
    public function delete (){
    $destributeurs = Client::where('typeclient','destributeur')->get();
        return view('adminlte::supprimerDestributeur',['destributeurs' => $destributeurs]);
    }
    public function update (){
         $destributeurs = Client::where('typeclient','destributeur')->get();

        return view('adminlte::modifierDestributeur',['destributeurs' => $destributeurs]);
    }
    public function pagedelete($id){
       $destributeur = Client::where('id', $id)->first();
        return view('adminlte::pagesupprimerDestributeur',['destributeur' => $destributeur]);
    }
     public function pageupdate($id){
            $destributeur = Client::where('id', $id)->first();

        return view('adminlte::pagemodifierDestributeur',['destributeur' => $destributeur]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'tel' => 'required',
              'password' => 'required',
              

            ]);
       $data=[
          'nom' => $request->input('nom'),
          'prenom' => $request->input('prenom'),
          'tel' => $request->input('tel'),
          'password' => bcrypt($request->input('password')),
         
          
          
       ];
      $id=$request->input('id');
       Client::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/destributeurs/list')->with('info','Destributeur modifieé');
    }
     public function remove(Request $request){
               Client::where('id',(int)$id)->delete();
               return redirect('/destributeurs/list')->with('info','Destributeur supprimeé ');

    }
}
