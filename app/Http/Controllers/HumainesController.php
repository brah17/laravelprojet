<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use Illuminate\Support\Facades\DB;

class HumainesController extends Controller
{
    public function index(){
	 	return view("adminlte::indexHumaine");
	 }
     public function list(){

      $humaines = DB::table('resources')
            ->get();

      return view('adminlte::listHumaine',['humaines' => $humaines]);
    }
    public function addHumaine(){
      
      
      return view("adminlte::ajouterHumaine");
    }
    public function create(Request $request){
      
       
        $type="humaine";
       
       // $statut=$request->input('statut');
       
        //$responsable=$request->input('responsable');
        
        
        
       $resource=new Resource;
       $resource->type=$type;
       
       
       $count = Resource::where('id', '>=', 0)->count();
       $count=$count+1;
       $resource->num=$count;
       $resource->save();
       

        

   
    return redirect('/humaines/list')->with('info','Humaine ajouté');
        
       



    }
    public function read($id){
      
           
        $humaine=Resource::find($id);
        
        return view('adminlte::lectureHumaine',['humaine' => $humaine]);
    }
    public function delete (){
      $humaines = DB::table('resources')
            ->get();
        return view('adminlte::supprimerHumaine',['humaines' => $humaines]);
    }
    public function update (){
      $humaines = DB::table('resources')
            ->get();

        return view('adminlte::modifierHumaine',['humaines' => $humaines]);
    }
    public function pagedelete($id){
        $humaine=Resource::find($id);
            
        return view('adminlte::pagesupprimerHumaine',['humaine' => $humaine]);
    }
     public function pageupdate($id){
       $resources = DB::table('resources')
            ->get();
            
        return view('adminlte::pagemodifierHumaine',['resources' => $resources]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'prix' => 'required',
              'date_fin' => 'required',
              

            ]);
       $data=[
          'prix' => $request->input('prix'),
          'date_fin' => $request->input('date_fin'),
          
         
          
          
       ];
      $id=$request->input('id');
       Resource::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/humaines/list')->with('info','Humaine modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Resource::where('id',(int)$id)->delete();
               return redirect('/humaines/list')->with('info','Humaine supprimeé ');

    }
}
