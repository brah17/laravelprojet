<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\DB;

class InstitutionController extends Controller
{
     public function index(){
	 	return view("adminlte::indexClient");
	 }
      public function list(){

      $institutions = DB::table('clients')
            ->get();

      return view('adminlte::listInstitution',['institutions' => $institutions]);
    }
    public function addInstitution(){
      
      return view("adminlte::ajouterInstitution");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'email' => 'required|email|max:60|unique:clients',
              'pays' => 'required',
              
            ]);
       
        $nom=$request->input('nom');
        $prenom=$request->input('prenom');
        $pays=$request->input('pays');
        $tel=$request->input('tel');
       // $statut=$request->input('statut');
        $email=$request->input('email');
    
       // $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
        
       $client=new Client;
       $client->nom=$nom;
       $client->prenom=$prenom;
       $client->email=$email;
       $client->tel=$tel;
       $client->pays=$pays;
       $client->typeclient="institution";
       $client->save();
       

        

   
    return redirect('/institutions/list')->with('info','Institution ajouté');
        
       



    }
    public function read($id){
     $institution = Client::where('id', $id)->first();
           

        
        return view('adminlte::lectureInstitution',['institution' => $institution]);
    }
    public function delete (){
    $institutions = Client::where('typeclient','institution')->get();
        return view('adminlte::supprimerInstitution',['institutions' => $institutions]);
    }
    public function update (){
         $institutions = Client::where('typeclient','institution')->get();

        return view('adminlte::modifierInstitution',['institutions' => $institutions]);
    }
    public function pagedelete($id){
       $institution = Client::where('id', $id)->first();
        return view('adminlte::pagesupprimerInstitution',['institution' => $institution]);
    }
     public function pageupdate($id){
            $institution = Client::where('id', $id)->first();

        return view('adminlte::pagemodifierInstitution',['institution' => $institution]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'nom' => 'required',
              'prenom' => 'required',
              'tel' => 'required',
              

            ]);
       $data=[
          'nom' => $request->input('nom'),
          'prenom' => $request->input('prenom'),
          'tel' => $request->input('tel'),
         
          
          
       ];
      $id=$request->input('id');
       Client::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/institutions/list')->with('info','Institution modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Client::where('id',(int)$id)->delete();
               return redirect('/institutions/list')->with('info','Institution supprimeé ');

    }
}
