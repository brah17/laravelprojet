<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;
class AgentController extends Controller
{
     public function index(){
	 	return view("adminlte::indexAgent");
	 }
      public function list(){

      $agents = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

      return view('adminlte::listAgent',['agents' => $agents]);
    }
    public function addAgent(){
      
      return view("adminlte::ajouterAgent");
    }
    public function create(Request $request){
      $this->validate(
              $request,[
              'name' => 'required',
              'email' => 'required',
              'password' => 'required',
              
            ]);
       
        $name=$request->input('name');
       // $statut=$request->input('statut');
        $email=$request->input('email');
        $password=$request->input('password');
        $passwordBcrypt= bcrypt($password);
        //$responsable=$request->input('responsable');
        
        
         $fields = [
            'name'     => $name,
            'email'    => $email,
            'password' => bcrypt($password),
        ];
       $agent=new User;
       $agent->name=$name;
       $agent->email=$email;
       $agent->password=$passwordBcrypt;
       $agent->save();
       $role=new Role;
       $role->id_user=$agent->id;
       $role->role_user="agent";
       $role->save();

        

   
    return redirect('/agents/list')->with('info','Agent ajouté');
        
       



    }
    public function read($id){
      $agents = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($agents->all() as $agent){
              if($agent->id_user==$id){
                $agentss=$agent;
              }
            }

        
        return view('adminlte::lectureAgent',['agentss' => $agentss]);
    }
    public function delete (){
    $agents = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
        return view('adminlte::supprimerAgent',['agents' => $agents]);
    }
    public function update (){
      $agents = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();

        return view('adminlte::modifierAgent',['agents' => $agents]);
    }
    public function pagedelete($id){
      $agents = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($agents->all() as $agent){
              if($agent->id_user==$id){
                $agentss=$agent;
              }
            }
        return view('adminlte::pagesupprimerAgent',['agentss' => $agentss]);
    }
     public function pageupdate($id){
      $agents = DB::table('users')
            ->leftJoin('roles', 'users.id', '=', 'roles.id_user')
            ->get();
            foreach($agents->all() as $agent){
              if($agent->id_user==$id){
                $agentss=$agent;
              }
            }
        return view('adminlte::pagemodifierAgent',['agentss' => $agentss]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'name' => 'required',
              'password' => 'required',
              

            ]);
       $data=[
          'name' => $request->input('name'),
          'password' => bcrypt($request->input('password')),
         
          
          
       ];
      $id=$request->input('id');
       User::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/agents/list')->with('info','Agent modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Role::where('id_user',(int)$id)->delete();
               User::where('id',(int)$id)->delete();
               return redirect('/agents/list')->with('info','Agent supprimeé ');

    }
}
