<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lieu;
class LieusController extends Controller
{
    public function add(){
    	return view('adminlte::addLieu');
    }
    public function create(Request $request){
    	 $lieu=new Lieu();
         
    
        $lieu->namePlace=$request->input('place_name');
        $lieu->lieu_dit=$request->input('lieu_dit');
        $lieu->zip=$request->input('zip');
        $lieu->adress=$request->input('adress');
        $lieu->pays=$request->input('pays');
        $lieu->longitude=$request->input('longitude');
        $lieu->latitude=$request->input('latitude');
        $lieu->ville=$request->input('ville');
        $lieu->save();
      return redirect('/lieus')->with('info','Lieu ajoouté');
    }
    public function read($id){
    	 $lieus=Lieu::find($id);
        
        return view('adminlte::readLieu',['lieus' => $lieus]);
    }
    public function index (){
    	$lieus=Lieu::where('id','>',0)->paginate(5);
        return view('adminlte::indexLieu',['lieus' => $lieus]);
    }
    public function delete (){
    	$lieus=Lieu::where('id','>',0)->paginate(5);
        return view('adminlte::deleteLieu',['lieus' => $lieus]);
    }
    public function update (){
    	$lieus=Lieu::where('id','>',0)->paginate(5);
        return view('adminlte::updateLieu',['lieus' => $lieus]);
    }
    public function pagedelete($id){
    	$lieus=Lieu::find($id);
        return view('adminlte::pagedelete',['lieus' => $lieus]);
    }
     public function pageupdate($id){
    	$lieus=Lieu::find($id);
        return view('adminlte::pageupdate',['lieus' => $lieus]);
    }
      public function edit(Request $request){
       $this->validate(
              $request,[
              'ville' => 'required',
              'pays' => 'required',
              'adress' => 'required',
              'lieu_dit' => 'required',
              'nameplace' =>'required',
              'zip' => 'required'
            ]);
       $data=[
          'nameplace' => $request->input('nameplace'),
          'zip' => $request->input('zip'),
          'lieu_dit' => $request->input('lieu_dit'),
          'adress' => $request->input('adress'),
          'pays' => $request->input('pays'),
          'ville' => $request->input('ville')
          
       ];
       $id=$request->input('id');
      
       Lieu::where('id',(int)$id)->update($data);
        
         
    
        
        return redirect('/lieus')->with('info','Lieu modifieé');
    }
     public function remove(Request $request){
               $id=$request->input('id');
               Lieu::where('id',(int)$id)->delete();
               return redirect('/lieus')->with('info','Lieu supprimé');

    }
}
