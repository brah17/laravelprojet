<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <?php if(! Auth::guest()): ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo e(Gravatar::get($user->email)); ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo e(Auth::user()->name); ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo e(trans('adminlte_lang::message.online')); ?></a>
                </div>
            </div>
        <?php endif; ?>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="<?php echo e(trans('adminlte_lang::message.search')); ?>..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
          
            <!-- Optionally, you can add icons to the links -->
            
              <li class="dropdown-header">Evenements</li>
            <li class="treeview">
                <a href="<?php echo e(url('/lieus')); ?>"><i class='fa fa-link'></i> <span>Lieus</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/lieus/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/lieus/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/lieus/delete')); ?>">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="<?php echo e(url('/events')); ?>"><i class='fa fa-link'></i> <span>Events</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/events/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/events/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/events/delete')); ?>">Delete</a></li>
                    <li><a href="<?php echo e(url('/events/calendar')); ?>">Calendar</a></li>
                </ul>
            </li>
            <li class="dropdown-header">Logistique</li>
            <li class="treeview">
                <a href="<?php echo e(url('/accueils')); ?>"><i class='fa fa-link'></i> <span>Accueil</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/accueils/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/accueils/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/accueils/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/securites')); ?>"><i class='fa fa-link'></i> <span>Securite</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/securites/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/securites/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/securites/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/hygieenes')); ?>"><i class='fa fa-link'></i> <span>Hygieene</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/hygieenes/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/hygieenes/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/hygieenes/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/restaurations')); ?>"><i class='fa fa-link'></i> <span>Restauration</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/restaurations/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/restaurations/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/restaurations/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/sonorisations')); ?>"><i class='fa fa-link'></i> <span>Sonirisation</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/sonorisations/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/sonorisations/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/sonorisations/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/lumieres')); ?>"><i class='fa fa-link'></i> <span>Lumieres</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/lumieres/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/lumieres/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/lumieres/delete')); ?>">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="<?php echo e(url('/animations')); ?>"><i class='fa fa-link'></i> <span>Animations</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/animations/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/animations/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/animations/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/transports')); ?>"><i class='fa fa-link'></i> <span>Transport</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/transports/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/transports/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/transports/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="dropdown-header">Resources</li>
            <li class="treeview">
                <a href="<?php echo e(url('/humaines')); ?>"><i class='fa fa-link'></i> <span>Humaines</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/humaines/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/humaines/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/humaines/delete')); ?>">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="<?php echo e(url('/materielles')); ?>"><i class='fa fa-link'></i> <span>Materielles</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/materielles/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/materielles/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/materielles/delete')); ?>">Delete</a></li>
                </ul>
            </li>
              <li class="dropdown-header">Promotteur</li>
             <li class="treeview">
                <a href="<?php echo e(url('/organisateurs/list')); ?>"><i class='fa fa-link'></i> <span>Organisateurs</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/organisateurs/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/organisateurs/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/organisateurs/delete')); ?>">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="<?php echo e(url('/mandataires/list')); ?>"><i class='fa fa-link'></i> <span>Mandataires</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/mandataires/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/mandataires/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/mandataires/delete')); ?>">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="<?php echo e(url('/producteurs/list')); ?>"><i class='fa fa-link'></i> <span>Producteurs</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/producteurs/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/producteurs/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/producteurs/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/intermittants/list')); ?>"><i class='fa fa-link'></i> <span>Intermittants</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/intermittants/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/intermittants/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/intermittants/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="dropdown-header">Clients</li>
             <li class="treeview">
                <a href="<?php echo e(url('/spectateurs/list')); ?>"><i class='fa fa-link'></i> <span>Spectateurs</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/spectateurs/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/spectateurs/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/spectateurs/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/agences/list')); ?>"><i class='fa fa-link'></i> <span>Agences</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/agences/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/agences/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/agences/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/distributeurs/list')); ?>"><i class='fa fa-link'></i> <span>Distributeurs</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/distributeurs/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/distributeurs/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/distributeurs/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/revendeurs/list')); ?>"><i class='fa fa-link'></i> <span>Revendeur</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/revendeurs/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/revendeurs/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/revendeurs/delete')); ?>">Delete</a></li>
                </ul>
            </li>
                        <li class="treeview">
                <a href="<?php echo e(url('/institutions/list')); ?>"><i class='fa fa-link'></i> <span>Institution</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/institutions/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/institutions/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/institutions/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="dropdown-header">Utilisateurs</li>
            <li class="treeview">
                <a href="<?php echo e(url('/admins/list')); ?>"><i class='fa fa-link'></i> <span>Administrateur</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/admins/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/admins/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/admins/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/superviseurs/list')); ?>"><i class='fa fa-link'></i> <span>Superviseur</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/superviseurs/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/superviseurs/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/superviseurs/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/agents/list')); ?>"><i class='fa fa-link'></i> <span>Agent</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/agents/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/agents/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/agents/delete')); ?>">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo e(url('/controleurs/list')); ?>"><i class='fa fa-link'></i> <span>Controleur</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('/controleurs/add')); ?>">Ajouter</a></li>
                    <li><a href="<?php echo e(url('/controleurs/update')); ?>">Modifier</a></li>
                    <li><a href="<?php echo e(url('/controleurs/delete')); ?>">Delete</a></li>
                </ul>
            </li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
