<?php $__env->startSection('htmlheader_title'); ?>
	Change Title here!
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

				<div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <form method="POST" action="<?php echo e(url('/events/create')); ?>">
             <?php echo e(csrf_field()); ?>

             <?php if(count($errors)>0): ?>
             <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <div class="alert alert-danger">
               <?php echo e($error); ?>

             </div>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
             <?php endif; ?>
             <?php if(session('info')): ?>
              <div class="alert alert-danger">
            <?php echo e(session('info')); ?>

            </div>
            <?php endif; ?>
  <div class="form-group">
    <input type="text" class="form-control" id="title" name="title" placeholder="Name Event">
  </div>
  <div class="form-group">
  </div>
   <div class="form-group">
   <label for="start_date">Start date</label>
 <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Start date">
</div> 
<div class="form-group">
<label for="end_date">End date</label>
 <input type="date" class="form-control" name="end_date" id="end_date" placeholder="End date">
</div>
<div class="form-group">
  <select name="lieu" class="form-control">
      <?php $__currentLoopData = $lieus->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lieu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <option value="<?php echo e($lieu->id); ?>"><?php echo e($lieu->nameplace); ?></option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </select>
</div>



 
  <button type="submit" class="btn btn-primary">Create</button>
  <a class="btn btn-danger" href="<?php echo e(url('/home')); ?>">Back</a>
</form>
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>