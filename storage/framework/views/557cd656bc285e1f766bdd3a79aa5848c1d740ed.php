<?php $__env->startSection('htmlheader_title'); ?>
    Change Title here!
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <table class="table">
                             <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nom</th>
      <th scope="col">Email</th>
      <th scope="col">Password</th>
    </tr>
  </thead>
  <tbody>
  <?php if( count($superviseurs) > 0 ): ?>
  <?php $__currentLoopData = $superviseurs->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $superviseur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
   <?php if($superviseur->role_user=='superviseur'): ?>
  <tr>
   
    <td>
    <a href="<?php echo e(url('/superviseurs/pagedelete/'.$superviseur->id_user)); ?>">
    <?php echo e($superviseur->id_user); ?>

    </a>

    </td>
    <td><?php echo e($superviseur->name); ?></td>
    <td><?php echo e($superviseur->email); ?></td>
    <td><?php echo e($superviseur->password); ?></td>
    
  </tr>
  <?php endif; ?>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   <?php endif; ?>
  </tbody>
        

</table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::layouts.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>