<?php $__env->startSection('htmlheader_title'); ?>
	Change Title here!
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

				<div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <br>
                        
                        <br>
                            <?php if(session('info')): ?>
                            <div class="col-mg-6 alert alert-success">
                        <?php echo e(session('info')); ?>

                        </div>
                        <?php endif; ?>

                        <div>
                        
                    
                        


                        <table class="table">
                             <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Start date</th>
      <th scope="col">End date</th>
    </tr>
  </thead>
  <tbody>
  <?php if( count($events) > 0 ): ?>
  <?php $__currentLoopData = $events->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
   
  <tr>
   
    <td>
    <a href="<?php echo e(url('/events/pagedelete/'.$event->id)); ?>">
    <?php echo e($event->id); ?>

    </a>

    </td>
    <td><?php echo e($event->title); ?></td>
    <td><?php echo e($event->start_date); ?></td>
    <td><?php echo e($event->end_date); ?></td>
    
  </tr>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   <?php endif; ?>
  </tbody>
        

</table>

                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>