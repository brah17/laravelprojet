<?php $__env->startSection('htmlheader_title'); ?>
    Change Title here!
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <br>
                        
                        <br>
                            <?php if(session('info')): ?>
                            <div class="col-mg-6 alert alert-success">
                        <?php echo e(session('info')); ?>

                        </div>
                        <?php endif; ?>

                        <div>
                        
                    
                        


                        <table class="table">
                             <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nom</th>
      <th scope="col">Email</th>
      <th scope="col">Password</th>
    </tr>
  </thead>
  <tbody>
  <?php if( count($humaines) > 0 ): ?>
  <?php $__currentLoopData = $humaines->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $humaine): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
   <?php if($humaine->type=='humaine'): ?>
  <tr>
   
    <td>
    <a href="<?php echo e(url('/humaines/pagedelete/'.$humaine->id)); ?>">
    <?php echo e($humaine->id); ?>

    </a>

    </td>
    <td><?php echo e($humaine->id); ?></td>
    <td><?php echo e($humaine->num); ?></td>
    <td><?php echo e($humaine->type); ?></td>
    
  </tr>
  <?php endif; ?>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   <?php endif; ?>
  </tbody>
        

</table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::layouts.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>