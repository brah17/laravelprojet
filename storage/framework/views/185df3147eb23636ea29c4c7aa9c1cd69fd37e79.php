<?php $__env->startSection('htmlheader_title'); ?>
	<?php echo e(trans('adminlte_lang::message.home')); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						
						
						
						
						<br>
						
						<br>
							<?php if(session('info')): ?>
							<div class="col-mg-6 alert alert-success">
						<?php echo e(session('info')); ?>

						</div>
						<?php endif; ?>

						<div>
						
    				
						


						<table class="table">
                             <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Adress</th>
      <th scope="col">Pays</th>
    </tr>
  </thead>
  <tbody>
  <?php if( count($lieus) > 0 ): ?>
  <?php $__currentLoopData = $lieus->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lieu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
   
  <tr>
   
  	<td>
    <a href="<?php echo e(url('/lieus/pageupdate/'.$lieu->id)); ?>">
  	<?php echo e($lieu->id); ?>

    </a>

  	</td>
  	<td><?php echo e($lieu->adress); ?></td>
  	<td><?php echo e($lieu->pays); ?></td>
  	
  	
  </tr>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   <?php endif; ?>
  </tbody>
   		

</table>

				</div>
					</div>
					
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
		
					
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::layouts.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>