<?php $__env->startSection('htmlheader_title'); ?>
	<?php echo e(trans('adminlte_lang::message.home')); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Home brahim</h3><br/><br>
						<table class="table">
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td><?php echo e($lieus->id); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Longitude</th>
      <td><?php echo e($lieus->longitude); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Latitude</th>
      <td><?php echo e($lieus->latitude); ?></td>
      
    </tr>
     <tr>
      <th scope="row">Name place</th>
      <td><?php echo e($lieus->nameplace); ?></td>
      
    </tr>
     <tr>
      <th scope="row">Lieu dit</th>
      <td><?php echo e($lieus->lieu_dit); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Zip</th>
      <td><?php echo e($lieus->zip); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Adress</th>
      <td><?php echo e($lieus->adress); ?></td>
      
    </tr>
     <tr>
      <th scope="row">Pays</th>
      <td><?php echo e($lieus->pays); ?></td>
      
    </tr>
     <tr>
      <th scope="row">Ville</th>
      <td><?php echo e($lieus->ville); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td><?php echo e($lieus->created_at); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td><?php echo e($lieus->updated_at); ?></td>
      
    </tr>
  </tbody>
</table>
						
						


						
						</div>
					</div>
					<div class="box-body">
						<?php echo e(trans('adminlte_lang::message.logged')); ?>. Start creating your amazing application!
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::layouts.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>