<?php $__env->startSection('htmlheader_title'); ?>
    Change Title here!
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <form method="POST" action="<?php echo e(url('/agents/edit')); ?>">
                        <?php echo e(csrf_field()); ?>

             <?php if(count($errors)>0): ?>
             <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <div class="alert alert-danger">
               <?php echo e($error); ?>

             </div>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
             <?php endif; ?>
              <div class="form-group">
    
    <input type="text" class="form-control" id="id" name="id" value="<?php echo $agentss->id_user ?>" placeholder="Id">
    
  </div>
  <div class="form-group">
    
    <input type="text" class="form-control" id="name" name="name" value="<?php echo $agentss->name ?>" placeholder="Name">
    
  </div>
   <div class="form-group">
    
    <input type="text" class="form-control" id="password" name="password" value="<?php echo $agentss->password ?>" placeholder="Password">
    
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::layouts.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>