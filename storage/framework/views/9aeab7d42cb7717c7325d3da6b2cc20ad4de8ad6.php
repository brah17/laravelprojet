<?php $__env->startSection('htmlheader_title'); ?>
    Change Title here!
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
     <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home brahim</h3><br/><br>
                        <table class="table">
                     
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td><?php echo e($institution->id); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Nom</th>
      <td><?php echo e($institution->nom); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Prenom</th>
      <td><?php echo e($institution->prenom); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Email</th>
      <td><?php echo e($institution->email); ?></td>
      
    </tr>
     <tr>
      <th scope="row">Password</th>
      <td><?php echo e($institution->password); ?></td>
      
    </tr>
     <tr>
      <th scope="row">Telephone</th>
      <td><?php echo e($institution->tel); ?></td>
      
    </tr>
     <tr>
      <th scope="row">Pays</th>
      <td><?php echo e($institution->pays); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td><?php echo e($institution->created_at); ?></td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td><?php echo e($institution->updated_at); ?></td>
      
    </tr>
  </tbody>
</table>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::layouts.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>