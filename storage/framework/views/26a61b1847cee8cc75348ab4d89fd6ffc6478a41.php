<?php $__env->startSection('htmlheader_title'); ?>
	<?php echo e(trans('adminlte_lang::message.home')); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<form method="POST" action="<?php echo e(url('/lieus/edit')); ?>">
						<?php echo e(csrf_field()); ?>

             <?php if(count($errors)>0): ?>
             <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <div class="alert alert-danger">
               <?php echo e($error); ?>

             </div>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
             <?php endif; ?>
  <div class="form-group">
  	<input type="hidden" name="id" id="id" value="<?php echo $lieus->id ?>">
  </div>           
  <div class="form-group">
    
    <input type="text" class="form-control" id="nameplace" name="nameplace" value="<?php echo $lieus->nameplace ?>" placeholder="Change name place">
    
  </div>
   <div class="form-group">
    
    <input type="text" class="form-control" id="lieu_dit" name="lieu_dit" value="<?php echo $lieus->lieu_dit ?>" >
    
  </div>
   <div class="form-group">
    
    <input type="text" class="form-control" id="zip" name="zip" value="<?php echo $lieus->zip ?>" >
    
  </div>
  <div class="form-group">
    
    <input type="text" class="form-control" id="pays" name="pays" value="<?php echo $lieus->pays ?>" >
    
  </div>
  <div class="form-group">
    
    <input type="text" class="form-control" id="ville" name="ville" value="<?php echo $lieus->ville ?>" >
    
  </div>
  <div class="form-group">
    
    <input type="text" class="form-control" id="adress" name="adress" value="<?php echo $lieus->adress ?>" placeholder="Lieu">
    
  </div> 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
						
						


						</div>
					</div>
					<div class="box-body">
						<?php echo e(trans('adminlte_lang::message.logged')); ?>. Start creating your amazing application!
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>