@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="container-fluid spark-screen">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Home brahim</h3><br/><br>
            
             <form method="POST" action="{{ url('/events/insert') }}">
             {{ csrf_field() }}
             @if(count($errors)>0)
             @foreach($errors->all() as $error)
             <div class="alert alert-danger">
               {{ $error }}
             </div>
             @endforeach
             @endif
             @if(session('info'))
              <div class="alert alert-danger">
            {{ session('info') }}
            </div>
            @endif
  <div class="form-group">
    <input type="text" class="form-control" id="title" name="title" placeholder="Name Event">
  </div>
  <div class="form-group">
  </div>
   <div class="form-group">
   <label for="start_date">Start date</label>
 <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Start date">
</div> 
<div class="form-group">
<label for="end_date">End date</label>
 <input type="date" class="form-control" name="end_date" id="end_date" placeholder="End date">
</div>



 
  <button type="submit" class="btn btn-primary">Create</button>
  <a class="btn btn-danger" href="{{ url('/home') }}">Back</a>
</form>
						{{ trans('adminlte_lang::message.logged') }}. Start creating your amazing application!
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
