@extends('adminlte::layouts.about')

@section('htmlheader_title')
	Change Title here!
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       <form method="POST" action="{{ url('/intermittants/create') }}">
             {{ csrf_field() }}
             @if(count($errors)>0)
             @foreach($errors->all() as $error)
             <div class="alert alert-danger">
               {{ $error }}
             </div>
             @endforeach
             @endif
             @if(session('info'))
              <div class="alert alert-danger">
            {{ session('info') }}
            </div>
            @endif
  <div class="form-group">
    <input type="text" class="form-control" id="name" name="name" placeholder="Nom complete Intermittant">
  </div>
  
   <div class="form-group">
   
 <input type="email" class="form-control" name="email" id="email" placeholder="Email">
</div> 
<div class="form-group">

 <input type="password" class="form-control" name="password" id="password" placeholder="Password">
</div>



 
  <button type="submit" class="btn btn-primary">Create</button>
  
</form>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
