@extends('adminlte::layouts.about')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						
						
						
						
						<br>
						
						<br>
							@if(session('info'))
							<div class="col-mg-6 alert alert-success">
						{{ session('info') }}
						</div>
						@endif

						<div>
						
    				
						


						<table class="table">
                             <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Adress</th>
      <th scope="col">Pays</th>
    </tr>
  </thead>
  <tbody>
  @if( count($lieus) > 0 )
  @foreach( $lieus->all() as $lieu)
   
  <tr>
   
  	<td>
    <a href="{{ url('/lieus/pageupdate/'.$lieu->id) }}">
  	{{ $lieu->id }}
    </a>

  	</td>
  	<td>{{ $lieu->adress }}</td>
  	<td>{{ $lieu->pays }}</td>
  	
  	
  </tr>
   @endforeach
   @endif
  </tbody>
   		

</table>

				</div>
					</div>
					
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
		
					
@endsection
