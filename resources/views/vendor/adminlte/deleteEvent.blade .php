@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						
						
						
						
						<br>
						
						<br>
							@if(session('info'))
							<div class="col-mg-6 alert alert-success">
						{{ session('info') }}
						</div>
						@endif

						<div>
						
    				
						


						<table class="table">
                             <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Start date</th>
      <th scope="col">End date</th>
    </tr>
  </thead>
  <tbody>
  @if( count($lieus) > 0 )
  @foreach( $events->all() as $event)
   
  <tr>
   
  	<td>
    <a href="{{ url('/events/pagedeleteEvent/'.$event->id) }}">
  	{{ $lieu->id }}
    </a>

  	</td>
  	<td>{{ $event->title }}</td>
  	<td>{{ $lieu->start_date }}</td>
  	<td>{{ $lieu->end_date }}</td>
  	
  </tr>
   @endforeach
   @endif
  </tbody>
   		

</table>

				</div>
					</div>
					
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
		
					
@endsection
