@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
     <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home brahim</h3><br/><br>
                        <table class="table">
                     
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $agence->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Nom</th>
      <td>{{ $agence->nom }}</td>
      
    </tr>
    <tr>
      <th scope="row">Prenom</th>
      <td>{{ $agence->prenom }}</td>
      
    </tr>
    <tr>
      <th scope="row">Email</th>
      <td>{{ $agence->email }}</td>
      
    </tr>
    
     <tr>
      <th scope="row">Telephone</th>
      <td>{{ $agence->tel }}</td>
      
    </tr>
     <tr>
      <th scope="row">Pays</th>
      <td>{{ $agence->pays }}</td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $agence->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $agence->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
@endsection
