@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
     <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home brahim</h3><br/><br>
                        <table class="table">
                     
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $revendeur->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Nom</th>
      <td>{{ $revendeur->nom }}</td>
      
    </tr>
    <tr>
      <th scope="row">Prenom</th>
      <td>{{ $revendeur->prenom }}</td>
      
    </tr>
    <tr>
      <th scope="row">Email</th>
      <td>{{ $revendeur->email }}</td>
      
    </tr>
     <tr>
      <th scope="row">Password</th>
      <td>{{ $revendeur->password }}</td>
      
    </tr>
     <tr>
      <th scope="row">Telephone</th>
      <td>{{ $revendeur->tel }}</td>
      
    </tr>
     <tr>
      <th scope="row">Pays</th>
      <td>{{ $revendeur->pays }}</td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $revendeur->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $revendeur->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
@endsection
