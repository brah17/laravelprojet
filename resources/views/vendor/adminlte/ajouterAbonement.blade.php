@extends('adminlte::layouts.about')
@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <form method="POST" action="{{ url('/abonements/create') }}">
             {{ csrf_field() }}
             @if(count($errors)>0)
             @foreach($errors->all() as $error)
             <div class="alert alert-danger">
               {{ $error }}
             </div>
             @endforeach
             @endif
             @if(session('info'))
              <div class="alert alert-danger">
            {{ session('info') }}
            </div>
            @endif
   <div class="form-group">
 <select class="form-control" name="client">
     @foreach($clients->all() as $client)
     <option value="{{ $client->id }}" >{{ $client->nom }}</option>
     @endforeach
 </select>
  </div>
  <div class="form-group">
  </div>
   <div class="form-group">
   <label for="start_date">Debut date</label>
 <input type="date" class="form-control" name="start_date" id="debut_date" placeholder="Start date">
</div> 
<div class="form-group">
<label for="end_date">Fin date</label>
 <input type="date" class="form-control" name="end_date" id="fin_date" placeholder="End date">
</div>
<div class="form-group">
    <input type="text" name="prix" id="prix" placeholder="Prix" class="form-control">
</div>



 
  <button type="submit" class="btn btn-primary">Create</button>
  <a class="btn btn-danger" href="{{ url('/home') }}">Back</a>
</form>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
