@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<form method="POST" action="{{ url('/lieus/edit/'.$lieus->id) }}">
						{{ csrf_field() }}
             @if(count($errors)>0)
             @foreach($errors->all() as $error)
             <div class="alert alert-danger">
               {{ $error }}
             </div>
             @endforeach
             @endif
  <div class="form-group">
    
    <input type="text" class="form-control" id="place_name" name="place_name" value="<?php echo $lieus->nameplace ?>" placeholder="Change name place">
    
  </div>
   <div class="form-group">
    
    <input type="text" class="form-control" id="lieu_dit" name="lieu_dit" value="<?php echo $lieus->lieu_dit ?>" placeholder="Change Lieu dit">
    
  </div>
   <div class="form-group">
    
    <input type="text" class="form-control" id="zip" name="zip" value="<?php echo $lieus->zip ?>" placeholder="Change Zip">
    
  </div>
  <div class="form-group">
    
    <input type="text" class="form-control" id="adress" name="adress" value="<?php echo $lieus->adress ?>" placeholder="Change adress">
    
  </div>
   <div class="form-group">
    
    <input type="text" class="form-control" id="pays" name="pays" value="<?php echo $lieus->pays ?>" placeholder="Change pays">
    
  </div>
   <div class="form-group">
    
    <input type="text" class="form-control" id="ville" name="ville" value="<?php echo $lieus->ville ?>" placeholder="Change ville">
    
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
						
						


						</div>
					</div>
					<div class="box-body">
						{{ trans('adminlte_lang::message.logged') }}. Start creating your amazing application!
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
