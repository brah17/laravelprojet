@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       <form method="POST" action="{{ url('/invitations/create') }}">
             {{ csrf_field() }}
             @if(count($errors)>0)
             @foreach($errors->all() as $error)
             <div class="alert alert-danger">
               {{ $error }}
             </div>
             @endforeach
             @endif
             @if(session('info'))
              <div class="alert alert-danger">
            {{ session('info') }}
            </div>
            @endif
  <div class="form-group">
 <select class="form-control" name="client">
     @foreach($clients->all() as $client)
     <option value="{{ $client->id }}" >{{ $client->nom }}</option>
     @endforeach
 </select>
  </div>
   <div class="form-group">
 <select class="form-control" name="event">
     @foreach($mevents->all() as $mevent)
     <option value="{{ $mevent->id }}" >{{ $mevent->nom }}</option>
     @endforeach
 </select>
  </div>
   <div class="form-group">
   
 <input type="text" class="form-control" name="prix" id="prix" placeholder="Prix">
</div> 
 
  <button type="submit" class="btn btn-primary">Create</button>
  
</form>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
