@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
     <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home brahim</h3><br/><br>
                        <table class="table">
                     
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $agentss->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Nom complete</th>
      <td>{{ $agentss->name }}</td>
      
    </tr>
    <tr>
      <th scope="row">Email</th>
      <td>{{ $agentss->email }}</td>
      
    </tr>
     <tr>
      <th scope="row">Password</th>
      <td>{{ $agentss->password }}</td>
      
    </tr>
     <tr>
      <th scope="row">Role</th>
      <td>{{ $agentss->role_user }}</td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $agentss->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $agentss->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
@endsection
