@extends('adminlte::layouts.about')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<form method="POST" action="{{ url('/lieus/remove') }}">
						{{ csrf_field() }}
             @if(count($errors)>0)
             @foreach($errors->all() as $error)
             <div class="alert alert-danger">
               {{ $error }}
             </div>
             @endforeach
             @endif
  <div class="form-group">
    vous voulez supprimer ce lieu
    <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $lieus->id ?>" placeholder="Change name place">
    
  </div>
  
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
						
						


						</div>
					</div>
					<div class="box-body">
						{{ trans('adminlte_lang::message.logged') }}. Start creating your amazing application!
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
