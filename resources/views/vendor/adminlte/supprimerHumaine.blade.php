@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <br>
                        
                        <br>
                            @if(session('info'))
                            <div class="col-mg-6 alert alert-success">
                        {{ session('info') }}
                        </div>
                        @endif

                        <div>
                        
                    
                        


                        <table class="table">
                             <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nom</th>
      <th scope="col">Email</th>
      <th scope="col">Password</th>
    </tr>
  </thead>
  <tbody>
  @if( count($humaines) > 0 )
  @foreach( $humaines->all() as $humaine)
   @if($humaine->type=='humaine')
  <tr>
   
    <td>
    <a href="{{ url('/humaines/pagedelete/'.$humaine->id) }}">
    {{ $humaine->id }}
    </a>

    </td>
    <td>{{ $humaine->id }}</td>
    <td>{{ $humaine->num }}</td>
    <td>{{ $humaine->type }}</td>
    
  </tr>
  @endif
   @endforeach
   @endif
  </tbody>
        

</table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
