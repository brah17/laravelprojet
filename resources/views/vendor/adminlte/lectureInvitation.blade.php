@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
     <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home brahim</h3><br/><br>
                        <table class="table">
                     
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $invitation->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Id Event</th>
      <td>{{ $invitation->idevent }}</td>
      
    </tr>
    <tr>
      <th scope="row">Id client</th>
      <td>{{ $invitation->idclient }}</td>
      
    </tr>
     <tr>
      <th scope="row">Num Ticket</th>
      <td>{{ $invitation->numbill }}</td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $invitation->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $invitation->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
@endsection
