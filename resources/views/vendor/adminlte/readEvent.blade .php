@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Home brahim</h3><br/><br>
						<table class="table">
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $lieus->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Longitude</th>
      <td>{{ $lieus->longitude }}</td>
      
    </tr>
    <tr>
      <th scope="row">Latitude</th>
      <td>{{ $lieus->latitude }}</td>
      
    </tr>
     <tr>
      <th scope="row">Name place</th>
      <td>{{ $lieus->nameplace }}</td>
      
    </tr>
     <tr>
      <th scope="row">Lieu dit</th>
      <td>{{ $lieus->lieu_dit }}</td>
      
    </tr>
    <tr>
      <th scope="row">Zip</th>
      <td>{{ $lieus->zip }}</td>
      
    </tr>
    <tr>
      <th scope="row">Adress</th>
      <td>{{ $lieus->adress }}</td>
      
    </tr>
     <tr>
      <th scope="row">Pays</th>
      <td>{{ $lieus->pays }}</td>
      
    </tr>
     <tr>
      <th scope="row">Ville</th>
      <td>{{ $lieus->ville }}</td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $lieus->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $lieus->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
						
						


						
						</div>
					</div>
					<div class="box-body">
						{{ trans('adminlte_lang::message.logged') }}. Start creating your amazing application!
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
