@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <form method="POST" action="{{ url('/distributeurs/edit') }}">
                        {{ csrf_field() }}
             @if(count($errors)>0)
             @foreach($errors->all() as $error)
             <div class="alert alert-danger">
               {{ $error }}
             </div>
             @endforeach
             @endif
              <div class="form-group">
    
    <input type="text" class="form-control" id="id" name="id" value="<?php echo $destributeur->id ?>" placeholder="Id">
    
  </div>
  <div class="form-group">
    
    <input type="text" class="form-control" id="nom" name="nom" value="<?php echo $destributeur->nom ?>" placeholder="Nom">
    
  </div>
  <div class="form-group">
    
    <input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo $destributeur->prenom ?>" placeholder="Prenom">
    
  </div>
   <div class="form-group">
    
    <input type="text" class="form-control" id="tel" name="tel" value="<?php echo $destributeur->tel ?>" placeholder="Telephone">
    
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
