@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
     <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home brahim</h3><br/><br>
                        <table class="table">
                     
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $ticket->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Id Event</th>
      <td>{{ $ticket->idevent }}</td>
      
    </tr>
    <tr>
      <th scope="row">Id client</th>
      <td>{{ $ticket->idclient }}</td>
      
    </tr>
     <tr>
      <th scope="row">Prix</th>
      <td>{{ $ticket->prix }}</td>
      
    </tr>
     <tr>
      <th scope="row">Num Ticket</th>
      <td>{{ $ticket->numbill }}</td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $ticket->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $ticket->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
@endsection
