@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
     <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home brahim</h3><br/><br>
                        <table class="table">
                     
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $superviseurss->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Nom complete</th>
      <td>{{ $superviseurss->name }}</td>
      
    </tr>
    <tr>
      <th scope="row">Email</th>
      <td>{{ $superviseurss->email }}</td>
      
    </tr>
     <tr>
      <th scope="row">Password</th>
      <td>{{ $superviseurss->password }}</td>
      
    </tr>
     <tr>
      <th scope="row">Role</th>
      <td>{{ $superviseurss->role_user }}</td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $superviseurss->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $superviseurss->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
@endsection
