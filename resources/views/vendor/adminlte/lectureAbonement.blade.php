@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
     <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home brahim</h3><br/><br>
                        <table class="table">
                     
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $abonement->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Code client</th>
      <td>{{ $abonement->nom }}</td>
      
    </tr>
    <tr>
      <th scope="row">Date debut</th>
      <td>{{ $abonement->date_debut }}</td>
      
    </tr>
    <tr>
      <th scope="row">Date fin</th>
      <td>{{ $abonement->date_fin }}</td>
      
    </tr>
     <tr>
      <th scope="row">Prix</th>
      <td>{{ $abonement->rix }}</td>
      
    </tr>
     <tr>
      <th scope="row">Num abonement</th>
      <td>{{ $abonement->numbill }}</td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $abonement->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $abonement->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
@endsection
