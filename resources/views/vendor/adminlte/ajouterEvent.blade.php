@extends('adminlte::page')

@section('htmlheader_title')
	Change Title here!
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

				<div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <form method="POST" action="{{ url('/events/create') }}">
             {{ csrf_field() }}
             @if(count($errors)>0)
             @foreach($errors->all() as $error)
             <div class="alert alert-danger">
               {{ $error }}
             </div>
             @endforeach
             @endif
             @if(session('info'))
              <div class="alert alert-danger">
            {{ session('info') }}
            </div>
            @endif
  <div class="form-group">
    <input type="text" class="form-control" id="title" name="title" placeholder="Name Event">
  </div>
  <div class="form-group">
  </div>
   <div class="form-group">
   <label for="start_date">Start date</label>
 <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Start date">
</div> 
<div class="form-group">
<label for="end_date">End date</label>
 <input type="date" class="form-control" name="end_date" id="end_date" placeholder="End date">
</div>
<div class="form-group">
  <select name="lieu" class="form-control">
      @foreach( $lieus->all() as $lieu)
      <option value="{{ $lieu->id }}">{{ $lieu->nameplace }}</option>
      @endforeach
  </select>
</div>



 
  <button type="submit" class="btn btn-primary">Create</button>
  <a class="btn btn-danger" href="{{ url('/home') }}">Back</a>
</form>
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection
