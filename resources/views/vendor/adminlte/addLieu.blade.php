<!DOCTYPE html>
<html>
  <head>
    <title>Place Autocomplete</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #adress,#latitude,#longitude,#ville,#pays,#zip,#place_name{
      	display: none;
      }
    </style>
  </head>
  <body>
  <form method="post" action="{{ url('/lieus/create') }}">
  {{ csrf_field() }}
<input id="adress" type="text" name="adress" style="width:600px;"/><br/>
<input type="text" id="latitude" name="latitude" placeholder="Latitude"/>
<input type="text" id="longitude" name="longitude" placeholder="Longitude"/><br>
<input type="text" id="ville" name="ville" placeholder="Ville"/>
<input type="text" id="pays" name="pays" placeholder="Pays"/>
<input type="text" id="zip" name="zip" placeholder="Zip"/><br>
<input type="text" name="place_name" id="place_name" placeholder="Name">
<input type="text" name="lieu_dit" id="lieu_dit" placeholder="Lieu a dit"><br>
<button>Save</button>
  </form>
						</div>
    <input id="pac-input" class="controls" type="text"
        placeholder="Enter a location">
    <div id="type-selector" class="controls">
      <input type="radio" name="type" id="changetype-all" checked="checked">
      <label for="changetype-all">All</label>

      <input type="radio" name="type" id="changetype-establishment">
      <label for="changetype-establishment">Establishments</label>

      <input type="radio" name="type" id="changetype-address">
      <label for="changetype-address">Addresses</label>

      <input type="radio" name="type" id="changetype-geocode">
      <label for="changetype-geocode">Geocodes</label>
    </div>
    <div id="map"></div>

    <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
var latitude=document.getElementById('latitude');
      var longitude=document.getElementById('longitude');
      var adress=document.getElementById('adress');
      var ville=document.getElementById('ville');
       var pays=document.getElementById('pays');
       var zip=document.getElementById('zip');
       var namePlace=document.getElementById('place_name');
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
        	longitude.value="";
          latitude.value="";
          adress.value="";
          ville.value="";
          pays.value="";
          zip.value="";
          namePlace.value="";
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

           adress.value=place.formatted_address;

           var res = place.formatted_address.split(",");
           pays.value=res[4];
            latitude.value=place.geometry.location.lat();
            longitude.value=place.geometry.location.lng();  
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
            
          }));
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            for(var i=0; i < place.address_components.length; i++)
{
    var component = place.address_components[i];
    if(component.types[0] == "postal_code")
    {
      zip.value=component.long_name;
       // console.log(component.long_name);
    }
   if (component.types[0] == "country"){ 
        pays.value=component.long_name;
   }
   if (component.types[0] === "locality") {
            ville.value=component.long_name;
           //  pays.value=component.short_name;
              
              
}

}
namePlace.value=place.name;
           
            

            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || ''),
              (place.address_components[3] && place.address_components[3].short_name || ''),
              (place.address_components[4] && place.address_components[4].short_name || ''),
              (place.address_components[5] && place.address_components[5].short_name || '')
            ].join(' ');
          }

          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key= AIzaSyDllJZHBIlYOWhGAdNyFTgf28sviEpPur0 &libraries=places&callback=initMap"
        async defer></script>
  </body>
</html>