@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
     <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home brahim</h3><br/><br>
                        <table class="table">
                     
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $destributeur->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Nom</th>
      <td>{{ $destributeur->nom }}</td>
      
    </tr>
    <tr>
      <th scope="row">Prenom</th>
      <td>{{ $destributeur->prenom }}</td>
      
    </tr>
    <tr>
      <th scope="row">Email</th>
      <td>{{ $destributeur->email }}</td>
      
    </tr>
     <tr>
      <th scope="row">Password</th>
      <td>{{ $destributeur->password }}</td>
      
    </tr>
     <tr>
      <th scope="row">Telephone</th>
      <td>{{ $destributeur->tel }}</td>
      
    </tr>
     <tr>
      <th scope="row">Pays</th>
      <td>{{ $destributeur->pays }}</td>
      
    </tr>
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $destributeur->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $destributeur->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
@endsection
