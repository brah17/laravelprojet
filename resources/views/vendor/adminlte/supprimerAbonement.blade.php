@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <br>
                        
                        <br>
                            @if(session('info'))
                            <div class="col-mg-6 alert alert-success">
                        {{ session('info') }}
                        </div>
                        @endif

                        <div>
                        
                    
                        


                        <table class="table">
                             <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Client</th>
      <th scope="col">Date debut</th>
      <th scope="col">Date fin</th>
    </tr>
  </thead>
  <tbody>
  @if( count($abonements) > 0 )
  @foreach( $abonements->all() as $abonement)
   
  <tr>
   
    <td>
    <a href="{{ url('/abonements/pagedelete/'.$abonement->id) }}">
    {{ $abonement->id }}
    </a>

    </td>
    <td>{{ $abonement->id }}</td>
    <td>{{ $abonement->date_debut }}</td>
    <td>{{ $abonement->date_fin }}</td>
    
  </tr>
 
   @endforeach
   @endif
  </tbody>
        

</table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
