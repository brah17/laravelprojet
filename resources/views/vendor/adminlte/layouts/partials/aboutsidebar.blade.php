<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
          
            <!-- Optionally, you can add icons to the links -->
            
              <li class="dropdown-header">Evenements</li>
            <li class="treeview">
                <a href="{{ url('/lieus')}}"><i class='fa fa-link'></i> <span>Lieus</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/lieus/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/lieus/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/lieus/delete') }}">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="{{ url('/events')}}"><i class='fa fa-link'></i> <span>Events</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/events/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/events/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/events/delete') }}">Delete</a></li>
                    <li><a href="{{ url('/events/calendar') }}">Calendar</a></li>
                </ul>
            </li>
            <li class="dropdown-header">Logistique</li>
            <li class="treeview">
                <a href="{{ url('/accueils')}}"><i class='fa fa-link'></i> <span>Accueil</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/accueils/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/accueils/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/accueils/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/securites')}}"><i class='fa fa-link'></i> <span>Securite</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/securites/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/securites/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/securites/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/hygieenes')}}"><i class='fa fa-link'></i> <span>Hygieene</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/hygieenes/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/hygieenes/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/hygieenes/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/restaurations')}}"><i class='fa fa-link'></i> <span>Restauration</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/restaurations/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/restaurations/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/restaurations/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/sonorisations')}}"><i class='fa fa-link'></i> <span>Sonirisation</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/sonorisations/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/sonorisations/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/sonorisations/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/lumieres')}}"><i class='fa fa-link'></i> <span>Lumieres</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/lumieres/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/lumieres/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/lumieres/delete') }}">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="{{ url('/animations')}}"><i class='fa fa-link'></i> <span>Animations</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/animations/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/animations/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/animations/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/transports')}}"><i class='fa fa-link'></i> <span>Transport</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/transports/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/transports/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/transports/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="dropdown-header">Resources</li>
            <li class="treeview">
                <a href="{{ url('/humaines')}}"><i class='fa fa-link'></i> <span>Humaines</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/humaines/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/humaines/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/humaines/delete') }}">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="{{ url('/materielles')}}"><i class='fa fa-link'></i> <span>Materielles</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/materielles/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/materielles/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/materielles/delete') }}">Delete</a></li>
                </ul>
            </li>
              <li class="dropdown-header">Promotteur</li>
             <li class="treeview">
                <a href="{{ url('/organisateurs/list')}}"><i class='fa fa-link'></i> <span>Organisateurs</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/organisateurs/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/organisateurs/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/organisateurs/delete') }}">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="{{ url('/mandataires/list')}}"><i class='fa fa-link'></i> <span>Mandataires</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/mandataires/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/mandataires/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/mandataires/delete') }}">Delete</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="{{ url('/producteurs/list')}}"><i class='fa fa-link'></i> <span>Producteurs</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/producteurs/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/producteurs/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/producteurs/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/intermittants/list')}}"><i class='fa fa-link'></i> <span>Intermittants</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/intermittants/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/intermittants/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/intermittants/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="dropdown-header">Clients</li>
             <li class="treeview">
                <a href="{{ url('/spectateurs/list')}}"><i class='fa fa-link'></i> <span>Spectateurs</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/spectateurs/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/spectateurs/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/spectateurs/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/agences/list')}}"><i class='fa fa-link'></i> <span>Agences</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/agences/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/agences/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/agences/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/distributeurs/list')}}"><i class='fa fa-link'></i> <span>Distributeurs</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/distributeurs/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/distributeurs/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/distributeurs/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/revendeurs/list')}}"><i class='fa fa-link'></i> <span>Revendeur</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/revendeurs/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/revendeurs/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/revendeurs/delete') }}">Delete</a></li>
                </ul>
            </li>
                        <li class="treeview">
                <a href="{{ url('/institutions/list')}}"><i class='fa fa-link'></i> <span>Institution</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/institutions/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/institutions/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/institutions/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="dropdown-header">Utilisateurs</li>
            <li class="treeview">
                <a href="{{ url('/admins/list')}}"><i class='fa fa-link'></i> <span>Administrateur</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/admins/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/admins/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/admins/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/superviseurs/list')}}"><i class='fa fa-link'></i> <span>Superviseur</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/superviseurs/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/superviseurs/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/superviseurs/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/agents/list')}}"><i class='fa fa-link'></i> <span>Agent</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/agents/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/agents/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/agents/delete') }}">Delete</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('/controleurs/list')}}"><i class='fa fa-link'></i> <span>Controleur</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/controleurs/add') }}">Ajouter</a></li>
                    <li><a href="{{ url('/controleurs/update') }}">Modifier</a></li>
                    <li><a href="{{ url('/controleurs/delete') }}">Delete</a></li>
                </ul>
            </li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
