@extends('adminlte::page')

@section('htmlheader_title')
	Change Title here!
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

				<div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <br>
                        
                        <br>
                            @if(session('info'))
                            <div class="col-mg-6 alert alert-success">
                        {{ session('info') }}
                        </div>
                        @endif

                        <div>
                        
                    
                        


                        <table class="table">
                             <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Start date</th>
      <th scope="col">End date</th>
    </tr>
  </thead>
  <tbody>
  @if( count($events) > 0 )
  @foreach( $events->all() as $event)
   
  <tr>
   
    <td>
    <a href="{{ url('/events/read/'.$event->id) }}">
    {{ $event->id }}
    </a>

    </td>
    <td>{{ $event->title }}</td>
    <td>{{ $event->start_date }}</td>
    <td>{{ $event->end_date }}</td>
    
  </tr>
   @endforeach
   @endif
  </tbody>
        

</table>
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection
