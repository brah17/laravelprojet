@extends('adminlte::page')

@section('htmlheader_title')
	Change Title here!
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

				<div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table">
  
  <tbody>
    <tr>
      <th scope="row">Id</th>
      <td>{{ $events->id }}</td>
      
    </tr>
    <tr>
      <th scope="row">Date de debut</th>
      <td>{{ $events->start_date }}</td>
      
    </tr>
    <tr>
      <th scope="row">Date de fini</th>
      <td>{{ $events->end_date }}</td>
      
    </tr>
     <tr>
      <th scope="row">Title</th>
      <td>{{ $events->title }}</td>
      
    </tr>
     <tr>
      <th scope="row">User</th>
      <td>{{ $events->user_id }}</td>
      
    </tr>
    
     
    <tr>
      <th scope="row">Creation</th>
      <td>{{ $events->created_at }}</td>
      
    </tr>
    <tr>
      <th scope="row">Last updated</th>
      <td>{{ $events->updated_at }}</td>
      
    </tr>
  </tbody>
</table>
                        
                    
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection
