@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <br>
                        
                        <br>
                            @if(session('info'))
                            <div class="col-mg-6 alert alert-success">
                        {{ session('info') }}
                        </div>
                        @endif

                        <div>
                        
                    
                        


                        <table class="table">
                             <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Evnement</th>
      <th scope="col">Client</th>
      <th scope="col">Prix</th>
    </tr>
  </thead>
  <tbody>
  @if( count($tickets) > 0 )
  @foreach( $tickets->all() as $ticket)
   
  <tr>
   
    <td>
    <a href="{{ url('/tickets/pageupdate/'.$ticket->id) }}">
    {{ $ticket->id }}
    </a>

    </td>
    <td>{{ $ticket->idevent }}</td>
    <td>{{ $ticket->idclient }}</td>
    <td>{{ $ticket->prix }}</td>
    
  </tr>

   @endforeach
   @endif
  </tbody>
        

</table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
