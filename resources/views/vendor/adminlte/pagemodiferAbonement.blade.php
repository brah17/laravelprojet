@extends('adminlte::layouts.about')

@section('htmlheader_title')
    Change Title here!
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Example box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                         <form method="POST" action="{{ url('/abonements/edit') }}">
                        {{ csrf_field() }}
             @if(count($errors)>0)
             @foreach($errors->all() as $error)
             <div class="alert alert-danger">
               {{ $error }}
             </div>
             @endforeach
             @endif
              <div class="form-group">
    
    <input type="text" class="form-control" id="id" name="id" value="<?php echo $abonement->id ?>" placeholder="Id">
    
  </div>
  <div class="form-group">
    
    <input type="date" class="form-control" id="date_debut" name="fin_debut" value="<?php echo $abonement->nom ?>" placeholder="Nom">
    
  </div>
  <div class="form-group">
    
    <input type="text" class="form-control" id="prix" name="prix" value="<?php echo $abonement->prix ?>" placeholder="Prix">
    
  </div>
  

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
