<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLieusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('lieus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nameplace',60);
            $table->string('lieu_dit',60);
            $table->string('zip',60);
            $table->string('adress',60);
            $table->string('pays',60);
            $table->string('longitude',60);
            $table->string('latitude',60);
            $table->string('ville',60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
